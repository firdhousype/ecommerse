from django.db import models
from django.conf import settings

# Create your models here.
class Push_Notifications(models.Model):
    From = models.EmailField ( max_length=70, null=True)
    Host = models.CharField(default=" ", max_length=70, null=True)
    Port = models.IntegerField(null=True)
    Username = models.CharField(default=" ", max_length=70, null=True)
    Password = models.CharField(default=" ", max_length=70, null=True)
    Subject = models.TextField(default=" ", max_length=700, null=True)
    Body = models.TextField(default=" ", null=True)
    
class SMS_Notifications(models.Model):
    From = models.EmailField ( max_length=70, null=True)
    Host = models.CharField(default=" ", max_length=70, null=True)
    Port = models.IntegerField(null=True)
    Username = models.CharField(default=" ", max_length=70, null=True)
    Password = models.CharField(default=" ", max_length=70, null=True)
    Subject = models.TextField(default=" ", max_length=700, null=True)
    Body = models.TextField(default=" ", null=True)
    
class Email_Notifications(models.Model):
    From = models.EmailField ( max_length=70, null=True)
    Host = models.CharField(default=" ", max_length=70, null=True)
    Port = models.IntegerField(null=True)
    Username = models.CharField(default=" ", max_length=70, null=True)
    Password = models.CharField(default=" ", max_length=70, null=True)
    Subject = models.TextField(default=" ", max_length=700, null=True)
    Body = models.TextField(default=" ", null=True)
