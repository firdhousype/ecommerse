from django.http import response
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from rest_framework.decorators import action
import requests
from django.template.loader import get_template
from xhtml2pdf import pisa
from io import BytesIO
from django.core.files.storage import FileSystemStorage
from Order.models import order,orderdetails
from Customer.models import User
from Product.models import *
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.contrib.auth import get_user_model
User = get_user_model()
from Ecom_Sku.utils import render_to_pdf
# Create your views here.
@login_required
def dashboard(request):
    total_order_details=order.objects.all().count()
    completed=order.objects.filter(status="Completed").count()
    accepted=order.objects.filter(status="Accepted").count()
    out_of_delivery=order.objects.filter(status="Out of Delivery").count()
    order_cancel=order.objects.filter(status="Order Cancel").count()
    customer=User.objects.filter(is_superuser=False).count()
    customer_list=User.objects.filter(is_superuser=False).order_by('-id')
    completed_list=order.objects.filter(status="Completed").order_by('-id')
    out_of_delivery_list=order.objects.filter(status="Out of Delivery").order_by('-id')
    accepted_list=order.objects.filter(status="Accepted").order_by('-id')
    
    order_cancel_list=order.objects.filter(status="Order Cancel").order_by('-id')
    orders=order.objects.all().order_by('-id')
    ordered=orders.filter(status='Ordered').count()
    user = request.user.username
    response = order.objects.all().order_by('-id')
    page=request.GET.get('page')
    paginator=Paginator(response,per_page=50)
    try:
        response=paginator.page(page)
    except PageNotAnInteger:
        response=paginator.page(1)
    except EmptyPage:
        response=paginator.page(paginator.num_pages)
    return render(request,'dashboard.html',
                            {'total_order_details':total_order_details,
                            'completed':completed,
                            'accepted':accepted,
                            'out_of_delivery':out_of_delivery,
                            'order_cancel':order_cancel,
                            'customers':customer,
                            'customer_list':customer_list,
                            'out_of_delivery_list':out_of_delivery_list,
                            'accepted_list':accepted_list,
                            'order_cancel_list':order_cancel_list,
                            'completed_list':completed_list,
                            'ordered':ordered,
                            'username':user,
                            'response':response,
                            'paginator':paginator})
def loginView(request):
    if request.method == 'POST':
        phone=request.POST.get('phone')
        password=request.POST.get('password')
        user = authenticate(username=phone, password=password)
        if  user is not None:
            login(request,user)
            k=User.objects.get(phone=phone)
            username=k.username
            return redirect(dashboard)
        else:
            extra = {'popup':True,'heading':'Login Failed','msg':'Invalid Phone Number OR Password'}
            return render(request,'login.html',{'extra':extra})
    return render(request,'login.html')

# #************************order acceptence and cancel ********************#
def accept(request):

    if 'action' in request.GET:
        order_id = request.GET.get('id')
        orders = order.objects.get(id = order_id)

        if orders.status == 'Ordered':
            if request.GET.get('action') =='1':
                orders.status = 'Accepted'
                orders.save()
                return redirect("dashboard")

            elif request.GET.get('action') == '0':
                orders.status = 'Order Cancel'
                orders.save()
                return redirect("dashboard")
        else:
            return redirect("dashboard")


        
        return redirect("dashboard")
    
# #********************************************show details**********************************************************#
def ShowDetails(request,id):
    
    orders = order.objects.get(id=id)
    print("xxxxxxxxxxxxxxxx",orders)
    orderd = orders.orderdetails_set.all().values()
    print("yyyyyyyyyyyyyyyyyyy",orderd)
    
    
    pro={}
    sku={}
    pvoop={}
    prefernce={}
    # if(orderd!=""): 
    if((orderd[0]['product_id_id'])!=None):
        proid=orderd[0]['product_id_id']
        pro=Product.objects.filter(id=proid).values()
        
        if((orderd[0]['sku_id'])!=None):
            skuid=orderd[0]['sku_id']
            sku=Sku.objects.filter(id=skuid).values()
            
            if((sku[0]['products_variation_combinations_id_id'])!=None): 
                per_id=sku[0]['products_variation_combinations_id_id']
                per=Products_variation_combinations.objects.filter(id=per_id).values()
                
                if((per[0]['product_variations_options_id_id']!=None)):
                    pvo=per[0]['product_variations_options_id_id']
                    pvoop=Products_variations_options.objects.filter(id=pvo).values()
                    
                    if((pvoop[0]['product_variation_id_id']!=None)): 
                        pvo1=pvoop[0]['product_variation_id_id']
                        prefernce=Products_variations.objects.filter(id=pvo1).values()
                         
                        return render(request,'invoice.html',{'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce})  
    
    return render(request,'invoice.html',{'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce})
#********************************************generate pdf *************************************************************#
def GeneratePDF(request,id):
    orders = order.objects.get(id=id)
    
    orderd = orders.orderdetails_set.all().values()
    
    
    
    pro={}
    sku={}
    pvoop={}
    prefernce={}
    if((orderd[0]['product_id_id'])!=None):
        proid=orderd[0]['product_id_id']
        pro=Product.objects.filter(id=proid).values()
        
        if((orderd[0]['sku_id'])!=None):
            skuid=orderd[0]['sku_id']
            sku=Sku.objects.filter(id=skuid).values()
            
            if((sku[0]['products_variation_combinations_id_id'])!=None): 
                per_id=sku[0]['products_variation_combinations_id_id']
                per=Products_variation_combinations.objects.filter(id=per_id).values()
                
                if((per[0]['product_variations_options_id_id']!=None)):
                    pvo=per[0]['product_variations_options_id_id']
                    pvoop=Products_variations_options.objects.filter(id=pvo).values()
                    
                    if((pvoop[0]['product_variation_id_id']!=None)): 
                        pvo1=pvoop[0]['product_variation_id_id']
                        prefernce=Products_variations.objects.filter(id=pvo1).values()
                         
                        template = get_template('invoice.html')
                        context = {'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce}
                        pdf = render_to_pdf('invoice.html',context)
                        if pdf:
                            response = HttpResponse(pdf,content_type='application/pdf')
                            filename= "Invoice_%s.pdf" %("12341231")
                            content = "inline; filename='%s'" %(filename)
                            if content :
                                response['Content-Disposition'] = 'attachment; filename="report.pdf"'
                                return response    
                            return HttpResponse("Not found")
    
    
    template = get_template('invoice.html')
    context = {'orders':orders,'orderd':orderd,'pro':pro,'sku':sku,'variance':pvoop,'preference':prefernce}
    
    pdf = render_to_pdf('invoice.html',context)
    
    if pdf:
        response = HttpResponse(pdf,content_type='application/pdf')
        
        filename= "Invoice_%s.pdf" %("12341231")
        
        content = "inline; filename='%s'" %(filename)
        
        if content :
            response['Content-Disposition'] = 'attachment; filename="report.pdf"'
            return response    
        return HttpResponse("Not found")

########## Notification ###
from django.shortcuts import render
from .models import *
# Create your views here.
def PushNotification(request):
    if request.method =='POST':
        From =request.POST.get('From')
        Host =request.POST.get('Host')
        Port =request.POST.get('Port')
        Username =request.POST.get('Username')
        Password =request.POST.get('Password')
        Subject =request.POST.get('Subject')
        Body =request.POST.get('Body')
        k=Push_Notifications.objects.create(From=From,Host=Host,Port=Port,Username=Username,Password=Password,Subject=Subject,Body=Body)
    return render(request, 'push.html')
def SMSNotification(request):
    if request.method =='POST':
        From =request.POST.get('From')
        Host =request.POST.get('Host')
        Port =request.POST.get('Port')
        Username =request.POST.get('Username')
        Password =request.POST.get('Password')
        Subject =request.POST.get('Subject')
        Body =request.POST.get('Body')
        k=SMS_Notifications.objects.create(From=From,Host=Host,Port=Port,Username=Username,Password=Password,Subject=Subject,Body=Body)
    return render(request, 'sms.html')
def EmailNotification(request):
    if request.method =='POST':
        From =request.POST.get('From')
        Host =request.POST.get('Host')
        Port =request.POST.get('Port')
        Username =request.POST.get('Username')
        Password =request.POST.get('Password')
        Subject =request.POST.get('Subject')
        Body =request.POST.get('Body')
        k=Email_Notifications.objects.create(From=From,Host=Host,Port=Port,Username=Username,Password=Password,Subject=Subject,Body=Body)
    return render(request, 'email.html')