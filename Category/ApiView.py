from django.http.response import HttpResponse
from django.shortcuts import render
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from rest_framework.response import Response 
from rest_framework.decorators import api_view,permission_classes
from django.core.files.storage import FileSystemStorage
from django.db.models import Q

from . models import *
from .serializers import  *
### List of all category exclude banners
@api_view(['GET'])
def categorylists(request):
    k=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def listcategorys(request):
    k=Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
### Subcategory list
@api_view(['GET'])
def subcategorylists(request):
    k=Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
    serializer=subserializer1(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def retrievecategory_byparents(request,catid):
    k=Category.objects.filter(parent__id=catid).exclude(category_name__contains="Banner").order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)

### Web banner list
@api_view(['GET'])
def bannerlists(request):
    k=Category.objects.filter(category_name__startswith='Banner').order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
### Mobile banner list
@api_view(['GET'])
def mobile_bannerlists(request):
    k=Category.objects.filter(category_name__startswith='Mobile_Banner').order_by('-id')
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def retrievecat(request,catid):
    k=Category.objects.filter(id=catid)
    serializer=pcatserializer(k,many=True)
    return Response(serializer.data)
