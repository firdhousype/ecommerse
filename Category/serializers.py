from rest_framework import serializers
from .models import *
########### category ##########
class subserializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields=[
           'category_name',
           ]
class subserializer1(serializers.ModelSerializer):
    parent=subserializer(read_only=True)
    class Meta:
        model=Category
        fields=[
            'id',
            'category_name',
            'category_slug',
            'category_image',
            'category_description',
           
            'parent'
           ]
        def get_related_field(self,model_field):
            return subserializer1()
class pcatserializer1(serializers.ModelSerializer):
    def to_representation(self,instance):
            my_fields={ 'id',
            'category_name',}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model=Category
        fields=[
            'id',
            'category_name',
            # 'category_slug',
            # 'category_image',
            # 'icon_image',
            # 'category_description',
            # 'vendor',
            #'parent'
        ]
class pcatserializer(serializers.ModelSerializer):
    parent=pcatserializer1(read_only=True)
    def to_representation(self,instance):
            my_fields={'id',
            'category_name',
            'category_slug',
            'category_image',
            'icon_image',
            'category_description',
            'parent',
            'mobile_banner',}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model=Category
        fields=[
            'id',
            'category_name',
            'category_slug',
            'category_image',
            'icon_image',
            'category_description',
            'parent',
            'mobile_banner',
        ]
        
class addOrganizationserializer(serializers.ModelSerializer):
    class Meta:
        model =addOrganization
        fields=['id','icon_image','company_name','phone','address'] 