from django.db import models
from django.contrib.auth.models import User
import PIL
from PIL import Image
from django.utils.safestring import mark_safe
from django.template.defaultfilters import truncatechars
from django.db.models import Avg

# Create your models here.
############# Catogory ##############
class Category(models.Model):
    parent           = models.ForeignKey('self',on_delete=models.CASCADE, blank=True, null=True,related_name='subcategories')
    category_name    = models.CharField(max_length=20)
    category_slug    = models.SlugField(max_length=20,blank=True,null=True)
    category_image   = models.ImageField(upload_to='categories',blank=True,null=True)
    icon_image       = models.ImageField(upload_to='categories',blank=True,null=True)
    category_description      = models.TextField(blank=True)
    mobile_banner             = models.ImageField(upload_to='categories',blank=True,null=True)
    variations=models.ManyToManyField('Product.Products_variations')
    variations_options=models.ManyToManyField('Product.Products_variations_options')
    def __str__(self):
        return str(self.id)
class addOrganization(models.Model):
    company_name=models.CharField(max_length=60,null=True,blank=True)
    phone=models.IntegerField(default=0,null=True,blank=True)
    address=models.TextField(blank=True)
    icon_image  = models.ImageField(upload_to='categories',blank=True,null=True)
    def __Str__(self):
        return self.company_name