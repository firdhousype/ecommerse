#Author : Firdhousy P.E
#Views.py : integration of microservices of category
from django.http import response
from django.shortcuts import render,redirect,HttpResponse
import requests
from pathlib import Path
import os
from django.conf import settings
from .models import *
from Product.models import *
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from django.core.files import File
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
import sys

def add_Category(request):
    categorys=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id')
    product_variations=Products_variations.objects.all()
    if 'load-options' in request.GET:
        variations          =   request.GET['load-options']
        value=variations.split(',')
        response            =   Products_variations_options.objects.filter(product_variation_id__in=value)
        return render(request,'variation_option.html',{'data':response})
    if request.method == 'POST':
        variations          =   request.POST.getlist('preference')
        variations1         =   Products_variations.objects.filter(id__in=variations)
        variations_options  =   request.POST.getlist('variance')
        variations_options1 =   Products_variations_options.objects.filter(id__in=variations_options)
        parent              =   request.POST.get('parent')
        category_name       =   request.POST.get('category_name')
        category_slug       =   request.POST.get('category_slug')
        category_description=   request.POST.get('category_description')
        category_image      =   request.FILES.get('category_image')
        icon_image          =   request.FILES.get('icon_image')
        if parent != "None":
            parent=Category.objects.get(id=parent)
            response=Category.objects.create(parent=parent,category_name=category_name,category_slug=category_slug,category_description=category_description,category_image=category_image,icon_image=icon_image)
            response.variations.set(variations1)
            response.save()
            response.variations_options.set(variations_options1)
            response.save()
            variations2 = [int(z) for z in variations]
            variations_options = [int(u) for u in variations_options]
            length = len(variations)
            i = 0
            while i < length:
                ids=Category.objects.get(id=response.id)
                variations=Products_variations.objects.get(id=variations2[i])
                response2=Products_options.objects.create(category_id=ids,product_variation_id=variations)
                k=Products_variations_options.objects.filter(product_variation_id__id=variations.id)
                k1=[]
                for y in k:
                    k1.append(y.id)
                list_as_set=set(k1)
                intersection=list_as_set.intersection(variations_options)
                intersection_as_lits=list(intersection)
                length1 = len(intersection_as_lits)
                j = 0
                while j < length1:
                    ids=Category.objects.get(id=response.id)
                    products_option_id=Products_options.objects.get(id=response2.id)
                    k2=Products_variations_options.objects.get(id=k1[j])
                    response3=Products_variation_combinations.objects.create(category_id=ids,product_option_id=products_option_id,product_variations_options_id=k2)
                    j += 1
                i += 1
            return redirect('categorylist')
        elif parent == "None":
            response=Category.objects.create(category_name=category_name,category_slug=category_slug,category_description=category_description,category_image=category_image,icon_image=icon_image)
            response.variations.set(variations1)
            response.save()
            response.variations_options.set(variations_options1)
            response.save()
            variations2 = [int(z) for z in variations]
            variations_options = [int(u) for u in variations_options]
            length = len(variations)
            i = 0
            while i < length:
                ids=Category.objects.get(id=response.id)
                variations=Products_variations.objects.get(id=variations2[i])
                response2=Products_options.objects.create(category_id=ids,product_variation_id=variations)
                k=Products_variations_options.objects.filter(product_variation_id__id=variations.id)
                k1=[]
                for y in k:
                    k1.append(y.id)
                list_as_set=set(k1)
                intersection=list_as_set.intersection(variations_options)
                intersection_as_lits=list(intersection)
                length1 = len(intersection_as_lits)
                j = 0
                while j < length1:
                    ids=Category.objects.get(id=response.id)
                    products_option_id=Products_options.objects.get(id=response2.id)
                    k2=Products_variations_options.objects.get(id=k1[j])
                    response3=Products_variation_combinations.objects.create(category_id=ids,product_option_id=products_option_id,product_variations_options_id=k2)
                    j += 1
                i += 1
            return redirect('categorylist')
    return render(request,'category.html',{'categorys':categorys,'product_variations':product_variations})
def combinations(request,catid):
    k=Products_variation_combinations.objects.filter(category_id=catid)
    return render(request,'combination.html',{'k':k})
def categorylist(request):
    categorys=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id').order_by('-id')
    if 'delete' in request.GET and request.method == 'POST':
        categoryid = request.POST.get('categoryid')
        retriveproduct=Product.objects.filter(Q(maincategory_id=categoryid)|Q(subcategory_id=categoryid)).values_list('id',flat=True)
        category=Category.objects.get(id=categoryid)
        product_option=Products_options.objects.filter(category_id=category)
        product_option.delete()
        product_variation_combination=Products_variation_combinations.objects.filter(category_id=category)
        product_variation_combination.delete()
        product_id=retriveproduct
        sku_delete=Sku.objects.filter(product_id__in=product_id)
        sku_delete.delete()
        pro_delete=Product.objects.filter(Q(maincategory_id=categoryid)|Q(subcategory_id=categoryid))
        pro_delete.delete()
        category.delete()
        return redirect('categorylist')
    return render(request,'categorylist.html',{'categorys':categorys})
def updatecategory(request,catid):
    if 'load-options' in request.GET:
        variations          =   request.GET['load-options']
        value=variations.split(',')
        response            =   Products_variations_options.objects.filter(product_variation_id__in=value)
        return render(request,'variation_option.html',{'data':response})
    categorys=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id')
    product_variations=Products_variations.objects.all()
    product_variations_name=Products_variations_options.objects.all()
    category=Category.objects.filter(id=catid).values()
    print("---------------------UPDATE CATE",category)
    category=Category.objects.filter(id=catid)
    pv1=[]
    pv1=category[0].variations.all().values_list('id',flat=True)
    pvn1=[]
    pvn1=category[0].variations_options.all().values_list('id',flat=True)
    return render(request,'category_update.html',{'product_variations_name':product_variations_name,'pvn1':pvn1,'pv1':pv1,'categorys':categorys,'category':category,'product_variations':product_variations})
def update_Category(request):
    updatecategory={}
    if request.method == 'POST':
        catid               =   request.POST.get('catid')
        category            =   Category.objects.filter(id=catid)
        pd                  =   Category.objects.get(id=catid)
        variations          =   request.POST.getlist('preference')
        variations1         =   Products_variations.objects.filter(id__in=variations)
        variations_options  =   request.POST.getlist('variance')
        variations_options1 =   Products_variations_options.objects.filter(id__in=variations_options)
        parent              =   request.POST.get('parent')
        category_name       =   request.POST.get('category_name')
        category_slug       =   request.POST.get('category_slug')
        category_description=   request.POST.get('category_description')
        category_image      =   request.FILES.get('category_image')
        k=pd.variations.all().values_list('id',flat=True)
        if k:
            pd.variations.remove(*k)
        pd.variations.set(variations1)
        pd.save()
        j=pd.variations_options.all().values_list('id',flat=True)
        if j:
            pd.variations_options.remove(*j)
        pd.variations_options.set(variations_options1)
        pd.save()
        icon_image          =   request.FILES.get('icon_image')
        if parent != 'None':
            cat=Category.objects.get(id=parent)
            updatecategory['parent']                =   cat
        if category_name:
            updatecategory['category_name']         =   category_name
        if category_slug:
            updatecategory['category_slug']         =   category_slug
        if category_description:
            updatecategory['category_description']  =   category_description
        if category_image:
            category_image=request.FILES['category_image']
            images = Image.open(category_image)
            width, height  = images.size
            if width > 350 and height > 360:
                images = images.convert('RGB')
                images = images.resize((320,340),Image.ANTIALIAS)
                output = BytesIO()
                images.save(output, format='JPEG', quality=90)
                output.seek(0)
                new_pic= InMemoryUploadedFile(output, 'ImageField',category_image.name,'image/jpeg',sys.getsizeof(output), None)
                c=FileSystemStorage()
                fname=c.save(category_image.name,new_pic)
                updatecategory["category_image"] = fname
            else:
                images = images.convert('RGB')
                images = images.resize((320,340),Image.ANTIALIAS)
                output = BytesIO()
                images.save(output, format='JPEG', quality=90)
                output.seek(0)
                new_pic= InMemoryUploadedFile(output, 'ImageField',category_image.name,'image/jpeg',sys.getsizeof(output), None)
                c=FileSystemStorage()
                fname=c.save(category_image.name,new_pic)
                updatecategory["category_image"] = fname
        if icon_image:
            updatecategory['icon_image']            =   icon_image
        category.update(**updatecategory)
        ids=category[0].id
        product_option=Products_options.objects.filter(category_id=ids)
        product_option.delete()
        product_variation_combination=Products_variation_combinations.objects.filter(category_id=ids)
        product_variation_combination.delete()
        variations2 = [int(z) for z in variations]
        variations_options = [int(u) for u in variations_options]
        length = len(variations)
        i = 0
        while i < length:
            ids=Category.objects.get(id=catid)
            variations=Products_variations.objects.get(id=variations2[i])
            response2=Products_options.objects.create(category_id=ids,product_variation_id=variations)
            k=Products_variations_options.objects.filter(product_variation_id__id=variations.id)
            k1=[]
            for y in k:
                k1.append(y.id)
            list_as_set=set(k1)
            intersection=list_as_set.intersection(variations_options)
            intersection_as_lits=list(intersection)
            length1 = len(intersection_as_lits)
            j = 0
            while j < length1:
                ids=Category.objects.get(id=catid)
                products_option_id=Products_options.objects.get(id=response2.id)
                k2=Products_variations_options.objects.get(id=k1[j])
                response3=Products_variation_combinations.objects.create(category_id=ids,product_option_id=products_option_id,product_variations_options_id=k2)
                j += 1
            i += 1
        return redirect('categorylist')

def addWebbanner(request):
    categorys=Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id') 
    subcategory=Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
    webbanners  =   Category.objects.filter(category_name__startswith='Banner').values()
    cat_name =  []
    for i in webbanners:
        name = i['category_name']
        cat_name.append(name)
    if 's' in request.GET:
        value = request.GET.get('s')
        if int(value) == 1:
            response = Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id') 
            return render(request,'cat1.html',{'data':response})
        elif int(value) == 2:
            response = Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
            return render(request,'cat2.html',{'data':response})
    if request.method =='POST':
        category_name=request.POST.get('b_name')
        category_image = request.FILES.get('image')
        parent =request.POST.get('cp')
        if int(parent) !=0 :
            parent=Category.objects.get(id=parent)
            response=Category.objects.create(category_name=category_name,parent=parent,category_image=category_image)
            return redirect('listWebbanners')
        else:
            response=Category.objects.create(category_name=category_name,category_image=category_image)
            return redirect('listWebbanners')
    return render (request,'banner.html',{'webbanners':categorys,'subcategory':subcategory,'cat_name':cat_name})
def listWebbanners(request):
    webbanners  =   Category.objects.filter(category_name__startswith='Banner').order_by('-id')
    if 'delete' in request.GET and request.method =='POST':
        bannerid = request.POST.get('bannerid')
        banners  =   Category.objects.get(id=bannerid)
        banners.delete()
        return redirect('listWebbanners')
    return render(request,'webbannerlist.html',{'webbanners':webbanners})
def updateWebbanner(request,bannerid):
    if 's' in request.GET:
        value = request.GET.get('s')
        if int(value) == 1:
            response = Category.objects.filter(parent=None).exclude(category_name__contains="Banner")
            print("=======================",response)
            return render(request,'cat1.html',{'data':response})            
        elif int(value) == 2:
            response = Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
            return render(request,'cat2.html',{'data':response})
    subcategory =   Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
    categorys   =   Category.objects.all().exclude(category_name__contains="Banner").order_by('-id') 
    category    =   Category.objects.filter(id=bannerid)
    pr          =   category[0].parent_id
    c = 0
    if pr != None:
        c=1  
        return render (request,'update_banner.html',{'categorys':categorys,'subcategory':subcategory,'category':category,'pr':pr,'c':c})
    return render (request,'update_banner.html',{'categorys':categorys,'subcategory':subcategory,'category':category,'pr':pr,'c':c})
def update_banner(request):
    updatewebbanner =   {}
    if request.method =='POST':
        bannerid = request.POST.get('bannerid')
        category=Category.objects.filter(id=bannerid)
        b_name=request.POST.get('b_name')
        parent =request.POST.get('cp')
        image = request.FILES.get('category_image')
        if b_name:
            updatewebbanner['category_name']    =   b_name
        if parent:
            parent  =   Category.objects.get(id=parent)
            updatewebbanner['parent']    =   parent
        if image:
            updatewebbanner['category_image']    =   image
        category.update(**updatewebbanner)
        return redirect('listWebbanners')
def addMobilebanner(request):
    categorys   =   Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id') 
    subcategory =   Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
    mobilebanners   =   Category.objects.filter(category_name__startswith='Mobile_Banner').values()
    cat_name =  []
    for i in mobilebanners:
        name = i['category_name']
        cat_name.append(name)
    if 's' in request.GET:
        value = request.GET.get('s')
        if int(value) == 1:
            response = Category.objects.filter(parent=None).exclude(category_name__contains="Banner")
            return render(request,'cat1.html',{'data':response})            
        elif int(value) == 2:
            response = Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
            return render(request,'cat2.html',{'data':response})
    if request.method =='POST':
        category_name=request.POST.get('b_name')
        mobile_banner = request.FILES.get('image')
        parent =request.POST.get('cp')
        if int(parent) !=0 :
            parent=Category.objects.get(id=parent)
            cat=Category.objects.filter(id=parent.id)
            cat.update(mobile_banner=mobile_banner)
            response=Category.objects.create(category_name=category_name,parent=parent,mobile_banner=mobile_banner)
            return redirect('listMobilebanners')
        else:
            response=Category.objects.create(category_name=category_name,mobile_banner=mobile_banner)
            return redirect('listMobilebanners')
    return render (request,'mobilebanner.html',{'categorys':categorys,'subcategory':subcategory,'cat_name':cat_name})
def listMobilebanners(request):
    mobilebanners   =   Category.objects.filter(category_name__startswith='Mobile_Banner').order_by('-id')
    if 'delete' in request.GET and request.method =='POST':
        bannerid = request.POST.get('bannerid')
        banners=Category.objects.get(id=bannerid)
        banners.delete()
        return redirect('listMobilebanners')
    return render(request,'mobilebannerlist.html',{'mobilebanners':mobilebanners})
def updateMobilebanner(request,bannerid):
    if 's' in request.GET:
        value = request.GET.get('s')
        if int(value) == 1:
            response = Category.objects.filter(parent=None).exclude(category_name__contains="Banner")
            return render(request,'cat1.html',{'data':response})            
        elif int(value) == 2:
            response = Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
            return render(request,'cat2.html',{'data':response})
    categorys   =   Category.objects.filter(parent=None).exclude(category_name__contains="Banner").order_by('-id') 
    subcategory =   Category.objects.filter(parent__isnull=False).exclude(category_name__contains="Banner")
    category    =   Category.objects.filter(id=bannerid)
    pr          =   category[0].parent_id
    c = 0
    if pr != None:
        c=2
        return render (request,'update_mobilebanner.html',{'category':category,'categorys':categorys,'subcategory':subcategory,'pr':pr,'c':c})
    return render (request,'update_mobilebanner.html',{'category':category,'categorys':categorys,'subcategory':subcategory,'pr':pr,'c':c})
def update_mobilebanner(request):
    updatemobbanner =   {}
    if request.method =='POST':
        bannerid = request.POST.get('bannerid')
        category=Category.objects.filter(id=bannerid)
        b_name=request.POST.get('b_name')
        parent=request.POST.get('cp')
        image = request.FILES.get('image')
        cat  =   Category.objects.filter(id=parent)
        cat.update(mobile_banner=image)
        if b_name:
            updatemobbanner['category_name']    =   b_name
        if parent:
            parent  =   Category.objects.get(id=parent)
            updatemobbanner['parent']    =   parent
        if image:
            updatemobbanner['mobile_banner']    =   image
        category.update(**updatemobbanner)
        return redirect('listMobilebanners')


def AddOrganization(request):
    
    if request.method=='POST':
        name=request.POST.get('name')
        
        phone=request.POST.get('phone')
        
        address=request.POST.get('address')
        
        
        im1=request.FILES["image"]
        f1=FileSystemStorage()
        fr1=f1.save(im1.name,im1)
        
        addOrganization.objects.create(company_name=name,phone=phone,address=address,icon_image=fr1)
        return redirect('listOrganization')    
        
    return render(request,'logo.html')
    

def ListOrganization(request):
    
    list_organization=addOrganization.objects.all()
    if 'delete' in request.GET and request.method =='POST':
        delorgid = request.POST.get('delorgid')
        delorg=addOrganization.objects.filter(id=delorgid)
        delorg.delete()
        return redirect('listOrganization')
    return render(request,'listorganization.html',{'list_organization':list_organization})




def UpdateOrganization(request,orgid):
    org=addOrganization.objects.filter(id=orgid).values()
    print("ttttttttttttttttt",org)
    
    if request.method =='POST':
        
       
        name=request.POST.get('name')
        
        
        phone=request.POST.get('phone')
        
        address=request.POST.get('address')
        
        im1=request.FILES["image"]
        f1=FileSystemStorage()
        fr1=f1.save(im1.name,im1)
        
        org.update(company_name=name,phone=phone,address=address,icon_image=fr1)
        return redirect('listOrganization')
    return render (request,"update_logo.html",{'org':org[0]})