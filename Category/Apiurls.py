from django.contrib import admin
from django.urls import path,include
from . import ApiView
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('categorylists',ApiView.categorylists,name='categorylists'), 
    path('maincategorys',ApiView.listcategorys,name='listcategorys'), 
    path('subcategorylists',ApiView.subcategorylists,name='subcategorylists'), 
    path('retrievecategory_un_maincat/<int:catid>',ApiView.retrievecategory_byparents,name='retrievecategory_byparents'), 
    path('bannerlists',ApiView.bannerlists,name='bannerlists'),
    path('mobile_bannerlists',ApiView.mobile_bannerlists,name='mobile_bannerlists'),
    path('retrievecat/<int:catid>',ApiView.retrievecat,name='retrievecat')
]