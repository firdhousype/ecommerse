from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    
    path('addcourier',views.addCourier,name='addCourier'),
    path('updateCourier/<int:courierid>',views.update_courier,name="updateCourier"),
    path('listCourier',views.listCourier,name="listCourier"),
    # path('deletecourier/<int:userid>',views.deletecourier,name="deletecourier"),
    # path('searchbyname',views.searchbyname,name="searchbyname"),
    # path('retrieve_courier/<int:cid>',views.retrieve_courier,name="retrieve_courier")
]