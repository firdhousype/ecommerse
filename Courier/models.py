from django.db import models

from django.db import models


class Courier(models.Model):
    name =models.CharField (max_length=50,null=True,blank=True)
    address = models.CharField(max_length=100,null=True,blank=True)
    phone = models.IntegerField(null=True,blank=True)
    city =models.CharField(max_length=50,null=True,blank=True)
    postcode = models.IntegerField(null=True,blank=True)
    website_url = models.CharField(max_length=100,null=True,blank=True,default="Null")

    def __str__(self):
        return str(self.name)

