
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from rest_framework.response import Response 
from . models import Couponcode


def addCoupon(request):
    if request.method == 'POST':
        coupon_name = request.POST.get('coupon_name')
        coupon      = request.POST.get('coupon')
        flat_value  =  request.POST.get('flat_value')
        response=Couponcode.objects.create(coupon_name=coupon_name,coupon=coupon,flat_value=flat_value)
        return redirect('listCoupon')
    return render(request,"coupon.html")

def listCoupon(request):
    coupon=Couponcode.objects.all().order_by('-id')
    if 'delete' in request.GET and request.method =='POST':
        couponid = request.POST.get('couponid')
        coupon=Couponcode.objects.filter(id=couponid)  
        return redirect('listCoupon')  
    return render(request,"listcoupon.html",{'coupon':coupon})

def updateCoupon(request,couponid):
    coupon_=Couponcode.objects.filter(id=couponid)
    return render(request,'coupon_update.html',{'coupon_':coupon_})

def update_Coupon(request):
    update_coupon={}
    if request.method == 'POST':
        couponid = request.POST.get('couponid')
        coupon_=Couponcode.objects.filter(id=couponid)
        flat_value  = request.data.get('flat_value')
        coupon_name = request.data.get('coupon_name')
        if coupon_name:
            update_coupon["coupon_name"]=coupon_name
        coupon      = request.data.get('coupon')
        if coupon:
            update_coupon["coupon"] = coupon
        if flat_value:
            update_coupon["flat_value"]=flat_value
        coupon_.update(**update_coupon)
        return redirect('listCoupon')
