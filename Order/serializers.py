from rest_framework import serializers
from .models import orderdetails,order
import requests
from Product.serializers import Skuserializer
from Product.models import Sku
class orderdeserializer2(serializers.ModelSerializer):
    coupon_code = serializers.CharField(source='coupon_code.coupon_name',allow_blank=True,allow_null=True)
    sku=serializers.SerializerMethodField('get_sku')
    def to_representation(self,instance):
            my_fields={'id',
                'orderDetails_id',
                'product_qty',
                'product_price',
                'subtotal',
                'product_id',
                'preference',
                'variance',
                'pincode',
                'coupon_code',
                'courier_id',
                'sku',}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model = orderdetails
        fields =  [
        'id',
        'orderDetails_id',
        'product_qty',
        'product_price',
        'subtotal',
        'product_id',
        'preference',
        'variance',
        'pincode',
        'coupon_code',
        'courier_id',
        'sku',
        ]
    def get_sku(self,obj):
        if obj.sku:
            k=Sku.objects.filter(sku_name=obj.sku)
            serializer = Skuserializer(k,many=True)
            return serializer.data
class orderserializer2(serializers.ModelSerializer):
    orderdetails = orderdeserializer2(source='orderdetails_set',many =True)
    def to_representation(self,instance):
            my_fields={'id',
                    'order_id',
                    'order_date',
                    'order_total',
                    'user',
                    'delivery_date',
                    'status',
                    'Is_delivered',
                    'Select_courier',
                    'payment_method',
                    'payment_status',
                    'orderdetails',}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model = order
        fields =   ['id',
        'order_id',
        'order_date',
        'order_total',
        'user',
        'delivery_date',
        'status',
        'Is_delivered',
        'Select_courier',
  
        'payment_method',
        'payment_status',
        'orderdetails',]
class orderserializer1(serializers.ModelSerializer):
    def to_representation(self,instance):
            my_fields={'id',
                    'order_id',
                    'order_date',
                    'order_total',
                    'user',
                    'delivery_date',
                    'status',
                    'Is_delivered',
                    'Select_courier',
            
                    'payment_method',
                    'payment_status',}
            data=super().to_representation(instance)
            for field in my_fields:
                try:
                    if not data[field]:
                        data[field]=""
                except KeyError:
                    pass 
            return data
    class Meta:
        model = order
        fields =   ['id',
        'order_id',
        'order_date',
        'order_total',
        'user',
        'delivery_date',
        'status',
        'Is_delivered',
        'Select_courier',
  
        'payment_method',
        'payment_status',
        ]