from django.contrib import admin
from django.urls import path,include
from . import Apiviews
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
############### ORDER CRUD OPERATIONS #################
    path('createorder_razorpay',Apiviews.createorder_razorpay,name='createorder_razorpay'),
    path('success',Apiviews.success,name='success'),
    path('createOrder',Apiviews.createOrder,name='createOrder'),
    path('updateorderTotal/<int:orderid>',Apiviews.updateorderTotal, name='updateorderTotal'),
    path('updateOrder<int:orderid>',Apiviews.updateOrder,name='updateOrder'),
    path('deleteOrder<int:orderid>',Apiviews.deleteOrder,name='deleteOrder'),
    path('updateorderdetails<int:orderdetailsid>',Apiviews.updateorderdetails,name='updateorderdetails'),
    path('deleteOrderDetails<int:orderdetailsid>',Apiviews.deleteOrderDetails,name='deleteOrderDetails'),
    path('listOrder',Apiviews.listOrder,name='listOrder'),
    path('listcompleteOrder',Apiviews.listcompleteOrder,name='listcompleteOrder'),
    path('listacceptedOrder',Apiviews.listacceptedOrder,name='listacceptedOrder'),
    path('listOutofDeliveryOrder',Apiviews.listOutofDeliveryOrder,name='listOutofDeliveryOrder'),
    path('listCancelOrder',Apiviews.listCancelOrder,name='listOutofDeliveryOrder'),
    path('listOrderdetails/<int:orderid>',Apiviews.listOrderdetails,name='listOrderdetails'),
    path('retrieveOrder/<int:orderid>',Apiviews.retrieveOrder,name="retrieveOrder"),
    path('salesreportsearch',Apiviews.salesreportsearch,name="salesreportsearch"),
    path('dashboardorders',Apiviews.dashboardoders,name="dashboardorders"),
    path('dashboardordertotal',Apiviews.dashboardordertotal,name="dashboardordertotal"),
    path('dashboardsalesvalue',Apiviews.dashboardsalesvalue,name="dashboardsalesvalue"),
    path('showdetails/<int:id>',Apiviews.Showdetails,name="showdetails"),
    # path('orderlistsearch',Apiviews.orderlistsearch,name="orderlistsearch"),

    
]
