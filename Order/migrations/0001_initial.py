# Generated by Django 3.2.4 on 2021-07-19 03:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_id', models.IntegerField(null=True)),
                ('order_id', models.CharField(blank=True, max_length=100, null=True)),
                ('order_date', models.DateField(null=True)),
                ('order_total', models.IntegerField(null=True)),
                ('payment_method', models.CharField(default=' ', max_length=50, null=True)),
                ('payment_status', models.BooleanField(blank=True, default=' ', null=True)),
                ('delivery_date', models.DateField(blank=True, null=True)),
                ('delivery_time', models.TimeField(blank=True, null=True)),
                ('status', models.CharField(blank=True, choices=[('Completed', 'Completed'), ('Ordered', 'Ordered'), ('Accepted', 'Accepted'), ('Order Cancel', 'Order Cancel'), ('Customer Cancel', 'Customer Cancel'), ('Delivered', 'Delivered'), ('Added to Cart', 'Added to Cart'), ('Out of Delivery', 'Out of Delivery'), ('Refund Initiated', 'Refund Initiated'), ('Return And exchange', 'Return And exchange')], default='Ordered', max_length=50, null=True)),
                ('Is_delivered', models.BooleanField(blank=True, default=False, null=True)),
                ('is_withrawn', models.BooleanField(blank=True, default=False, null=True)),
                ('paid', models.CharField(blank=True, choices=[('Yes', 'YES'), ('No', 'NO')], default='NO ', max_length=50, null=True)),
                ('pay_to_vendor', models.BooleanField(blank=True, default=False, null=True)),
                ('delivery_tracking_id', models.CharField(blank=True, default=' ', max_length=50, null=True)),
                ('customerstatus_changedate', models.DateField(blank=True, null=True)),
                ('notify', models.BooleanField(default=False, null=True)),
                ('Select_courier', models.CharField(blank=True, default=' ', max_length=50, null=True)),
                ('vendor_id', models.IntegerField(blank=True, null=True)),
                ('commission', models.IntegerField(blank=True, default=0, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='orderdetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('orderDetails_id', models.CharField(blank=True, max_length=100, null=True)),
                ('product_id', models.IntegerField(blank=True, null=True)),
                ('Net_weight', models.CharField(blank=True, default='', max_length=50, null=True)),
                ('commission', models.IntegerField(blank=True, null=True)),
                ('product_qty', models.FloatField(null=True)),
                ('product_price', models.IntegerField(null=True)),
                ('subtotal', models.IntegerField(null=True)),
                ('preference', models.CharField(default=' ', max_length=50, null=True)),
                ('variance', models.CharField(default=' ', max_length=50, null=True)),
                ('pincode', models.IntegerField(blank=True, null=True)),
                ('comment', models.TextField(blank=True, null=True)),
                ('coupon_code', models.CharField(default=' ', max_length=50, null=True)),
                ('courier_id', models.CharField(blank=True, max_length=100, null=True)),
                ('sku', models.IntegerField(blank=True, null=True)),
                ('order', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='Order.order')),
            ],
        ),
    ]
