from .models import orderdetails,order
from Customer.models import User
from Product.models import Product
from django.http import response
from django.shortcuts import redirect, render,HttpResponse
from django.conf import settings
import calendar
import time
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from Courier.models import *
def listOrders(request):
    response = order.objects.all().order_by('-id')
    page=request.GET.get('page')
    paginator=Paginator(response,per_page=50)
    try:
        response=paginator.page(page)
    except PageNotAnInteger:
        response=paginator.page(1)
    except EmptyPage:
        response=paginator.page(paginator.num_pages)
    return render(request,"listorder.html",{'response':response})
def update_order(request,orderid):
    orders = order.objects.filter(id=orderid)
    courier=Courier.objects.all()
    details = orderdetails.objects.all()
    return render(request,"updateorder.html",{'response':response,'order':orders,'courier':courier,'details':details})
def updateOrders(request):  
    if request.method == 'POST':  
        updatevalues={}        
        order_date=request.POST.get('order_date')
        if order_date:
            updatevalues["order_date"]=order_date
        order_total=request.POST.get('order_total')
        if order_total:
            updatevalues["order_total"]=order_total
        delivery_date=request.POST.get('delivery_date')
        if delivery_date:
            updatevalues["delivery_date"]=delivery_date
        status = request.POST.get('status')
        if status:
            updatevalues["status"]=status
        Is_delivered=request.POST.get('Is_delivered') 
        if Is_delivered == "Yes":
            Is_delivered = True
            updatevalues["Is_delivered"]=Is_delivered
        elif Is_delivered == "No":
            Is_delivered =  False
            updatevalues["Is_delivered"]=Is_delivered
        payment_method=request.POST.get('payment_method')
        if payment_method:
            updatevalues["payment_method"]=payment_method
        payment_status=request.POST.get('payment_status')
        if payment_status:
            if payment_status == "Yes":
                payment_status = True
                updatevalues["payment_status"]=payment_status
            elif payment_status == "No":
                payment_status =  False
                updatevalues["payment_status"]=payment_status
        order_detail_id=request.POST.getlist('combination_id[]')
        Select_courier= request.POST.getlist('combination_id1[]')
        orderid = request.POST.get('orderid')
        if order_detail_id and Select_courier:
            k=len(Select_courier)
            L1=[]
            L1.append(order_detail_id)
            L1.append(Select_courier)
            N = len(L1)
            j=0
            while j < int(k):
                res = [i[j] for i in L1[ : N]]
                order_detail_id=res[0]
                Select_courier=res[1]
                Order_details = orderdetails.objects.filter(order=orderid).filter(orderDetails_id=order_detail_id)
                Order_details.update(courier_id=Select_courier)
                updatevalues['Select_courier']=Select_courier 
                j += 1        
        k = order.objects.filter(id=orderid).values()
        k.update(**updatevalues )
        return redirect('listOrders')
def ordersearch(request):
    response=order.objects.all()
    if request.method=='POST':
        s1=request.POST.get("startingdate")
        s2=request.POST.get("endingdate")
        status1=request.POST.get("status")
        customer_name=request.POST.get("customer_name")
        Product_name = request.POST.get("Product_name")
        if s1 and s2  and status1 and customer_name and Product_name :
            response = order.objects.filter(order_date__lte=s1,order_date__gte=s2,status=status1,user__id=customer_name,product_id__product_name__startswith=Product_name)
            return render(request,"listorder.html",{'response':response})
        elif s1 and s2  and status1 and customer_name:
            response = order.objects.filter(order_date__lte=s1,order_date__gte=s2,status=status1,user__id=customer_name)
            return render(request,"listorder.html",{'response':response})
        elif s1 and s2  and status1 :
            response = order.objects.filter(order_date__lte=s1,order_date__gte=s2,status=status1)
            return render(request,"listorder.html",{'response':response})
        elif s1 and s2 :
            response = order.objects.filter(order_date__lte=s1,order_date__gte=s2)
            return render(request,"listorder.html",{'response':response})
        elif s1 :
            response = order.objects.filter(order_date__lte=s1)
            return render(request,"listorder.html",{'response':response})
        elif s2  :
            response = order.objects.filter(order_date__gte=s2)
            return render(request,"listorder.html",{'response':response})
        elif status1 :
            print("lopopopopjdxbcv xbz")
            response = order.objects.filter(status=status1)
            return render(request,"listorder.html",{'response':response})
        elif customer_name:
            response = order.objects.filter(user__id=customer_name)
            return render(request,"listorder.html",{'response':response})
        elif  Product_name :
            response = order.objects.filter(product_id__product_name__startswith=Product_name)
            return render(request,"listorder.html",{'response':response})
        elif status1 and customer_name:
            response = order.objects.filter(status=status1,user__id=customer_name)
            return render(request,"listorder.html",{'response':response})
        elif s1  and status1  :
            response = order.objects.filter(status=status1,order_date__lte=s1)
            return render(request,"listorder.html",{'response':response})
        elif  s2   and status1 :
            response = order.objects.filter(status=status1,order_date__gte=s2)
            return render(request,"listorder.html",{'response':response})
        elif(s1 and s2 )==''and status1=='' and customer_name=='' and Product_name !="":
            response = order.objects.filter(product_id__in=Product.objects.filter(product_name__contains=Product_name))
            ls=[]
            for i in response:
                ls.append(i.order_id)
            a = map(lambda x:order.objects.get(id=x),ls)
            return render(request,"listorder.html",{'response':response})
    return render(request,"listorder.html",{'response':response})