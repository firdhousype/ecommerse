# Author--FirdhousyP.E
# order crud operation are done here
from django.shortcuts import render,redirect, resolve_url
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
# from rest_framework.authtoken.admin import User
from rest_framework.response import Response 
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view,permission_classes
from .serializers import orderdeserializer2,orderserializer2,orderserializer1
from .models import orderdetails,order
from Customer.models import *
from Product.models import *
from .models import orderdetails,order
from django.conf import settings
import calendar
import time
import requests
from django.db.models import Sum, aggregates
import razorpay,datetime
from django.contrib.auth import get_user_model
User = get_user_model()

client =  razorpay.Client(auth = ("rzp_test_HrTbJZnmrvCn2i","bOxvxWY8GfsLCHpmcyA3X868"))
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createorder_razorpay(request):
    if request.method == "POST":
            order_date=request.data.get('orderDate')
            order_total = request.data.get('orderTotal')
            delivery_date=request.data.get('deliveryDate')
            status = request.data.get('status')
            Is_delivered=request.data.get('isDelivered')
            payment_method=request.data.get('paymentMethod')
            payment_status=request.data.get('paymentStatus')
            customer_id=request.data.get('customerId')
            d = requests.get(user_url+'retrieveUser'+str(customer_id)).json()
            customerstatus_changedate=request.data.get('customerstatus_changedate')
            vendor_id=request.data.get('vendorId')
            order_currency=request.data.get('currency')
            listorderdetails = request.data.get('orderdetails')
            for i in listorderdetails:
                product_id=i.get('productId')
                Net_weight=i.get('netWeight')
                product_qty=i.get('productQuantity')
                pincode=i.get('pincode')
                skuid=i.get('skuId')
                product_price=i.get('productPrice')
                comment=i.get('comment')
            order_amount = int(order_total)*100
            notes ={'Shipping address': d[0]['address']}
            order_receipt = datetime.date.today().strftime('%D')
            # CREAING ORDER in Razor pay
            response = client.order.create(dict(amount=order_amount, currency=order_currency, receipt=order_receipt, notes=notes, payment_capture='1'))
            print('--------------Razor Pay order is created ----------------',response)
            order_id = response['id']
            order_status = response['status']
            amount=response['amount']
            if order_status=='created':
                data={
                    "razorpay_id":order_id,
                    "status":order_status
                    }
                return Response(data)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def success(request):
    if request.method == "POST":
        userid=request.data.get('customerId')
        d = User.objects.get(id=userid)
        order_date=request.data.get('orderDate')
        order_total = request.data.get('orderTotal')
        payment_method=request.data.get('paymentMethod')
        payment_status=request.data.get('paymentStatus')
        delivery_date=request.data.get('deliveryDate')
        status = request.data.get('status')
        Is_delivered=request.data.get('isDelivered')
        customerstatus_changedate=request.data.get('customerstatus_changedate')
        vendor_id=request.data.get('vendorId')
        razorpay_order_id=request.data.get('razorpay_order_id')
        razorpay_payment_id=request.data.get('razorpay_payment_id')
        razorpay_signature=request.data.get('razorpay_signature')
        orderCreationId=request.data.get('orderCreationId')
        params_dict = {
            'orderCreationId':orderCreationId,
            'razorpay_order_id' : razorpay_order_id,
            'razorpay_payment_id' : razorpay_payment_id,
            'razorpay_signature' : razorpay_signature
            }
        try:
            status = client.utility.verify_payment_signature(params_dict)
            # gmt stores current gmtime
            gmt = time.gmtime()
            # ts stores timestamp
            ts = calendar.timegm(gmt)
            order_id = 'OR' +str(userid) +str(ts)
            if customerstatus_changedate:
                orderId = order.objects.create(order_id=order_id,vendor_id=vendor_id,customer_id=d,order_date=order_date,order_total=order_total,payment_method=payment_method,payment_status=payment_status,delivery_date=delivery_date,status=status,Is_delivered=Is_delivered,customerstatus_changedate=customerstatus_changedate)
            else:
                orderId = order.objects.create(order_id=order_id,vendor_id=vendor_id,customer_id=d,order_date=order_date,order_total=order_total,payment_method=payment_method,payment_status=payment_status,delivery_date=delivery_date,status=status,Is_delivered=Is_delivered)
            listorderdetails = request.data.get('orderdetails')
            for i in listorderdetails:
                product_id=i.get('productId')
                p = Product.objects.get(id=product_id)
                Net_weight=i.get('netWeight')
                product_qty=i.get('productQuantity')
                pincode=i.get('pincode')
                skuid=i.get('skuId')
                product_price=i.get('productPrice')
                comment=i.get('comment')
                # gmt stores current gmtime
                gmt = time.gmtime()
                # ts stores timestamp
                ts = calendar.timegm(gmt)
                orderDetails_id = "OD"+str(userid)+str(product_id)+str(ts)
                qty=int(Net_weight)*int(product_qty)
                subtotal= int(qty)*int(price)
                ############### stcock reducing part ############
                sku=Sku.objects.filter(id=skuid,product_id=p)
                price=sku[0].price
                stock=sku[0].stock
                newstock=int(stock)-int(qty)
                sku.update(stock=newstock)
                skus=Sku.objects.get(id=skuid)
                orderdetails.objects.create(orderDetails_id=orderDetails_id,product_id=p,sku=skus,product_qty=product_qty,pincode=pincode,product_price=product_price,subtotal=subtotal,order=orderId,Net_weight=Net_weight,comment=comment)
            ad = order.objects.filter(id=orderId.id)
            serializer = orderserializer2(ad,many=True)
            return Response(serializer.data)
        except:
            data={'status': 'Payment Faliure!!!'}
            return Response(data)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def createOrder(request):
    # vendor_mobile=0
    if request.method == "POST":
        userid=request.data.get('customerId')
        d = User.objects.get(id=userid)
        order_date=request.data.get('orderDate')
        order_total = request.data.get('orderTotal')
        payment_method=request.data.get('paymentMethod')
        payment_status=request.data.get('paymentStatus')
        delivery_date=request.data.get('deliveryDate')
        status = request.data.get('status')
        Is_delivered=request.data.get('isDelivered')
        customerstatus_changedate=request.data.get('customerstatus_changedate')
        vendor_id=request.data.get('vendorId')
        # gmt stores current gmtime
        gmt = time.gmtime()
        # ts stores timestamp
        ts = calendar.timegm(gmt)
        order_id = 'OR' +str(userid) +str(ts)
        if customerstatus_changedate:
            orderId = order.objects.create(order_id=order_id,vendor_id=vendor_id,user=d,order_date=order_date,order_total=order_total,payment_method=payment_method,payment_status=payment_status,delivery_date=delivery_date,status=status,Is_delivered=Is_delivered,customerstatus_changedate=customerstatus_changedate)
        else:
            orderId = order.objects.create(order_id=order_id,vendor_id=vendor_id,user=d,order_date=order_date,order_total=order_total,payment_method=payment_method,payment_status=payment_status,delivery_date=delivery_date,status=status,Is_delivered=Is_delivered)
        listorderdetails = request.data.get('orderdetails')
        for i in listorderdetails:
            product_id=i.get('productId')
            p = Product.objects.get(id=product_id)
            Net_weight=i.get('netWeight')
            product_qty=i.get('productQuantity')
            pincode=i.get('pincode')
            skuid=i.get('skuId')
            product_price=i.get('productPrice')
            comment=i.get('comment')
            # gmt stores current gmtime
            gmt = time.gmtime()
            # ts stores timestamp
            ts = calendar.timegm(gmt)
            orderDetails_id = "OD"+str(userid)+str(product_id)+str(ts)
            sku=Sku.objects.filter(id=skuid,product_id=p)
            price=sku[0].price
            qty=int(Net_weight)*int(product_qty)
            subtotal= int(qty)*int(price)
            skus=Sku.objects.get(id=skuid)
            # ############### stcock reducing part ############
            # sku=Sku.objects.filter(id=skuid,product_id=p)
            # price=sku[0].price
            # stock=sku[0].stock
            # newstock=int(stock)-int(qty)
            # sku.update(stock=newstock)
            orderdetails.objects.create(orderDetails_id=orderDetails_id,product_id=p,sku=skus,product_qty=product_qty,pincode=pincode,product_price=product_price,subtotal=subtotal,order=orderId,Net_weight=Net_weight,comment=comment)
    ad = order.objects.filter(id=orderId.id)
    serializer = orderserializer2(ad,many=True)
    return Response(serializer.data)
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def updateorderTotal(request,orderid):
    k = order.objects.filter(id=orderid).values()
    orderObj=order.objects.filter(id=orderid)
    if request.method == 'PUT':
        updatevalues={}
        order_date=request.data.get('orderDate')
        if order_date:
            updatevalues["order_date"]=order_date
        order_total=request.data.get('orderTotal')
        if order_total:
            updatevalues["order_total"]=order_total
        delivery_date=request.data.get('deliveryDate')
        if delivery_date:
            updatevalues["delivery_date"]=delivery_date
        status = request.data.get('status')
        if status:
            updatevalues["status"]=status
        Is_delivered=request.data.get('isDelivered')
        if Is_delivered:
            updatevalues["Is_delivered"]=Is_delivered
        payment_method=request.data.get('paymentMethod')
        if payment_method:
            updatevalues["payment_method"]=payment_method
        payment_status=request.data.get('paymentStatus')
        if payment_status:
            updatevalues["payment_status"]=payment_status
        customerstatus_changedate=request.data.get('customerstatus_changedate')
        if customerstatus_changedate:
            updatevalues["customerstatus_changedate"]=customerstatus_changedate
        m = order.objects.filter(id=orderid)
        m.update(**updatevalues )
        ordObj=order.objects.get(id=orderid)
        ad = order.objects.all()
        serializer = orderserializer1(ad,many=True)
        listorderdetails = request.data.get('orderdetails')
        orderList = orderdetails.objects.filter(order_id=orderid)
        print (listorderdetails)
        for i in listorderdetails:
            updateOrderValues={}
            try:
                orderdetailsid=i.get('id')
            except:
                orderdetailsid=0
            product_id=i.get('productId')
            p = Product.objects.get(id=product_id)
            if product_id:
                updateOrderValues["product_id"]=product_id
                Net_weight=i.get('netWeight')
                product_qty=i.get('productQuantity')
                pincode=i.get('pincode')
                skuid=i.get('skuId')
                product_price=i.get('productPrice')
                comment=i.get('comment')
                qty=int(Net_weight)*int(product_qty)
                subtotal= int(qty)*int(product_price)
                ############### stcock reducing part ############
                sku=Sku.objects.filter(id=skuid,product_id=p)
                price=sku[0].price
                stock=sku[0].stock
                newstock=int(stock)-int(qty)
                sku.update(stock=newstock)
                if sku[0].stock > 0:
                    orderUpdateList = orderdetails.objects.filter(id=orderdetailsid)
                    if subtotal:
                        updateOrderValues["subtotal"]=subtotal
                    if product_qty:
                        updateOrderValues["product_qty"]=product_qty
                    if Net_weight:
                        (updateOrderValues)["Net_weight"]=Net_weight
                    if pincode:
                        (updateOrderValues)["pincode"]=pincode
                    if orderdetailsid:
                        orderUpdateList.update(**updateOrderValues)
                else:
                    return Response("Out off stock")
        ad = order.objects.filter(id=orderid)
        serializer = orderserializer2(ad,many=True)
        return Response(serializer.data)
@api_view(['GET'])
def listOrder(request):
    k = order.objects.all()
    serializer = orderserializer2(k,many=True)
    return Response(serializer.data)
@api_view(['PUT'])
# @permission_classes([IsAuthenticated])
def updateOrder(request,orderid):      
    if request.method == 'PUT': 
        if order.objects.filter(id=orderid).exists():
            k = order.objects.filter(id=orderid)
            updatevalues={}        
            
            order_date=request.data.get('order_date')
            if order_date:
                updatevalues["order_date"]=order_date
            order_total=request.data.get('order_total')
            if order_total:
                updatevalues["order_total"]=order_total
            delivery_date=request.data.get('delivery_date')
            if delivery_date:
                updatevalues["delivery_date"]=delivery_date
            status = request.data.get('status')
            if status:
                updatevalues["status"]=status
            Is_delivered=request.data.get('Is_delivered')
            if Is_delivered:
                updatevalues["Is_delivered"]=Is_delivered
            payment_method=request.data.get('payment_method')
            if payment_method:
                updatevalues["payment_method"]=payment_method
            payment_status=request.data.get('payment_status')
            if payment_status:
                updatevalues["payment_status"]=payment_status
            vendor_id=request.data.get('vendor_id')
            if vendor_id:
                updatevalues["vendor_id"]=vendor_id
            order_detail_id = request.data.get('order_detail_id')
            Select_courier=request.data.get('Select_courier')
            if order_detail_id and Select_courier:
                Order_details = orderdetails.objects.filter(order=orderid).filter(orderDetails_id=order_detail_id)
                Order_details.update(courier_id=Select_courier)
                updatevalues['Select_courier']=Select_courier

            k.update(**updatevalues )
            serializer = orderserializer1(k,many=True)
            return Response(serializer.data)
        else:
            return Response("Invalid order id")
@api_view(['GET'])
def deleteOrder(request,orderid):
    name = order.objects.get(id=orderid)
    name.delete()
    return Response("user"+str(orderid)+"is deleted")
@api_view(['PUT'])
def updateorderdetails(request,orderdetailsid):
    if request.method == 'PUT':
        if orderdetails.objects.filter(id=orderdetailsid).exists():
            l = orderdetails.objects.filter(id=orderdetailsid)
            updatevalues={}
            product_id=request.data.get('productId')
            product_qty=request.data.get('productQuantity')
            product_price=request.data.get('productPrice')
            commission = request.data.get('commission')
            preference=request.data.get('preference')
            pincode=request.data.get('pincode')
            variance=request.data.get('variance')
            if product_id:
                updatevalues["product_id"]=product_id
            if product_qty:
                updatevalues["product_qty"]=product_qty
            if commission:
                updatevalues["commission"]=commission
            if product_price:
                updatevalues["product_price"]=product_price
            if variance:
                updatevalues["variance"]=variance
            if preference:
                updatevalues["preference"]=preference
            if pincode:
                updatevalues["pincode"]=pincode            
            l.update(**updatevalues )
            ad = orderdetails.objects.all()
            serializer = orderdeserializer2(l,many=True)
            return Response(serializer.data)
        else:
            return Response("Invalid orderdetails id")
@api_view(['GET'])
def deleteOrderDetails(request,orderdetailsid):
    orderDetails = orderdetails.objects.get(id=orderdetailsid)
    orderDetails.delete()
    return Response("Order details "+str(orderdetailsid)+" deleted successfully")

@api_view(['GET'])
def retrieveOrder(request,orderid):
    name = order.objects.filter(id=orderid)
    serializer = orderserializer1(name,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def listOrderdetails(request,orderid):
    k = orderdetails.objects.filter(order=orderid)
    serializer = orderdeserializer2(k,many=True)
    return Response(serializer.data)
#################### for integration #################
@api_view(['GET'])
def listcompleteOrder(request):
    k = order.objects.filter(status="Completed")
    serializer = orderserializer2(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def listacceptedOrder(request):
    k = order.objects.filter(status="Accepted")
    serializer = orderserializer2(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def listOutofDeliveryOrder(request):
    k = order.objects.filter(status="Out of Delivery")
    serializer = orderserializer2(k,many=True)
    return Response(serializer.data)
@api_view(['GET'])
def listCancelOrder(request):
    k = order.objects.filter(status="Order Cancel")
    serializer = orderserializer2(k,many=True)
    return Response(serializer.data)


@api_view(['POST','GET'])
def salesreportsearch(request):
    print("workin salesreportsearch working")
    orders=order.objects.all()
    serializer=orderserializer1(orders,many=True)
    if request.method=='POST':
        s1=request.data.get("L1")
        s2=request.data.get("L2")
        status1=request.data.get("status")
        if (s1 and s2 )!=''and status1=='':
            orders = order.objects.filter(order_date__range=(s1,s2))
            serializer=orderserializer1(orders,many=True)
            res=serializer.data
        elif (s1 and s2 )==''and status1!='':
            orders = order.objects.filter(status__contains=status1)
            serializer=orderserializer1(orders,many=True)
            res=serializer.data
        elif(s1 and s2 )!=''and status1!='':
            orders = order.objects.filter(status__contains=status1,order_date__range=(s1,s2))
            serializer=orderserializer1(orders,many=True)
            res=serializer.data
        return Response (res)
    return Response ("TEST")
@api_view(['GET'])
def dashboardoders(request): 
    k = order.objects.all().values()    
    orders_count=order.objects.all().count()
    completed_count=order.objects.filter(status="Completed").all().count()
    accepted_count=order.objects.filter(status="Accepted").all().count()
    out_for_delivery_count=order.objects.filter(status="Out of Delivery").all().count()
    order_cancel_count=order.objects.filter(status="Order Cancel").all().count()
    data={'k':k,'orders_count':orders_count,'completed_count':completed_count,'accepted_count':accepted_count,'out_for_delivery_count':out_for_delivery_count,'order_cancel_count':order_cancel_count}
    return Response(data)
@api_view(['GET'])
def dashboardordertotal(request): 
    order_totals=order.objects.aggregate(Sum('order_total'))
    return Response(order_totals)
@api_view(['GET'])
def dashboardsalesvalue(request): 
    subtotal=orderdetails.objects.aggregate(Sum('subtotal'))
    return Response(subtotal)


@api_view(['GET'])
def Showdetails(request,id):
    k = order.objects.filter(id=id)
    serializer = orderserializer2(k,many=True)
    return Response(serializer.data)




# @api_view(['POST','GET'])
# def orderlistsearch(request):
    # orders=order.objects.all()
    # serializer=orderserializer2(orders,many=True)
    # if request.method=='POST':
    #     s1=request.data.get("startingdate")
    #     s2=request.data.get("endingdate")
    #     status1=request.data.get("status")
    #     customer_id=request.data.get("customer_id")
    #     Product_id = request.data.get("Product_id")
    #     if (s1 and s2 )!='' and status1=='' and customer_id=='' and Product_id=='' :
    #         orders = order.objects.filter(order_date__range=(s1,s2))
    #         serializer=orderserializer2(orders,many=True)

    #         res= serializer.data
    #     elif (s1 and s2 )==''and status1!='' and customer_id=='' and Product_id=='' :
    #         orders = order.objects.filter(status=status1)
    #         serializer=orderserializer2(orders,many=True)

    #         res= serializer.data
    #     elif(s1 and s2 )==''and status1=='' and customer_id!='' and Product_id=='' :
    #         orders=order.objects.filter(customer_id=customer_id)
    #         serializer=orderserializer2(orders,many=True)
    #         res= serializer.data

    #     elif(s1 and s2 )!=''and status1!='' and customer_id==''  and Product_id=='':
    #         orders=order.objects.filter(status=status1,order_date__range=(s1,s2))
    #         serializer=orderserializer2(orders,many=True)
    #         res=serializer.data
    #     elif(s1 and s2 )!=''and status1=='' and customer_id!=''  and Product_id=='':
    #         orders=order.objects.filter(order_date__range=(s1,s2),customer_id=customer_id)
    #         serializer=orderserializer2(orders,many=True)
    #         res=serializer.data
    #     elif(s1 and s2 )!=''and status1!='' and customer_id!=''  and Product_id=='':
    #         orders=order.objects.filter(order_date__range=(s1,s2),customer_id=customer_id,status=status1)
    #         serializer=orderserializer2(orders,many=True)
    #         res=serializer.data
    #     elif(s1 and s2 )==''and status1=='' and customer_id=='' and Product_id !="":
    #         orders = orderdetails.objects.filter(product_id=Product_id)
    #         print(orders)
    #         ls=[]
    #         for i in orders:
    #             ls.append(i.order_id)
    #         a = map(lambda x:order.objects.get(id=x),ls)
    #         serializer = orderserializer2(a,many=True)
    #         res=serializer.data
    #     return Response (res)
    # return Response ("TEST")

