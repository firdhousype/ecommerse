from django.db import models
from Customer.models import *
class orderdetails(models.Model):
    orderDetails_id = models.CharField(max_length=100,null=True,blank=True)
    product_id = models.ForeignKey('Product.Product', on_delete = models.CASCADE, null=True)
    Net_weight = models.CharField(default="",max_length=50,null=True,blank=True)
    commission = models.IntegerField(null=True,blank=True)
    product_qty = models.FloatField(null=True)
    product_price = models.IntegerField(null=True)
    subtotal = models.IntegerField(null=True)
    preference=models.CharField(default=" ", max_length=50, null=True)
    variance=models.CharField(default=" ", max_length=50, null=True)
    pincode = models.IntegerField(null=True,blank=True)
    order = models.ForeignKey('order', on_delete = models.CASCADE, null=True)
    comment=models.TextField(null=True,blank=True)
    # coupon_code = models.CharField(default=" ", max_length=50, null=True)
    courier_id =models.ForeignKey('Courier.Courier',on_delete=models.SET_NULL,null=True,blank=True)
    sku=models.ForeignKey('Product.Sku',on_delete=models.SET_NULL,null=True,blank=True)
    coupon_code = models.ForeignKey('Coupon.Couponcode',on_delete=models.SET_NULL,null=True,blank=True)
    product_id = models.ForeignKey('Product.Product', on_delete = models.CASCADE, null=True)
    
    def __str__(self):
        return str(self.id)
class order(models.Model):
    STATUS = (
        ('Completed', 'Completed'),
        ('Ordered', 'Ordered'),
        ('Accepted', 'Accepted'),
        ('Order Cancel', 'Order Cancel'),
        ('Customer Cancel', 'Customer Cancel'),
        ('Delivered', 'Delivered'),
        ('Added to Cart', 'Added to Cart'),
        ('Out of Delivery', 'Out of Delivery'),
        ('Refund Initiated','Refund Initiated'),
        ('Return And exchange','Return And exchange')
        # ('Pending', 'Pending'),
        # ('Out for Delivery', 'Out for Delivery'),
    )

    PAID = (
        ('Yes','YES'),
        ('No','NO')
    )
    #customer_id = models.IntegerField(null=True)
    order_id = models.CharField(max_length=100,null=True,blank=True)
    order_date= models.DateField(null=True)
    order_total = models.IntegerField(null=True)
    payment_method = models.CharField(default=" ", max_length=50, null=True)
    payment_status = models.BooleanField(default=False, null=True,blank=True)
    delivery_date= models.DateField(null=True,blank=True)
    delivery_time=models.TimeField(null=True,blank=True)
    status = models.CharField(default="Ordered",max_length=50,null=True, choices=STATUS,blank=True)
    Is_delivered = models.BooleanField(default=False,null=True,blank=True)
    is_withrawn = models.BooleanField(default=False,null=True,blank=True)
    paid = models.CharField(default="NO ", max_length=50, null=True, choices=PAID,blank=True)
    pay_to_vendor = models.BooleanField(default=False,null=True,blank=True)
    delivery_tracking_id=models.CharField(default=" ", max_length=50, null=True,blank=True)
    customerstatus_changedate = models.DateField(null=True,blank=True)
    notify = models.BooleanField(default=False,null=True)
    Select_courier=models.CharField(default=" ", max_length=50, null=True,blank=True)
    vendor_id = models.IntegerField(null=True,blank=True)
    commission = models.IntegerField(default=0,null=True,blank=True)
    product_id = models.ForeignKey('Product.Product', on_delete = models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE, null=True)
    # vendor_id = models.ForeignKey('vendor.Vendor', on_delete = models.CASCADE, null=True,blank=True)
    # admin_commisiion = models.ForeignKey('vendor.Commission', on_delete = models.CASCADE, null=True)
    Select_courier=models.ForeignKey('Courier.Courier',on_delete = models.CASCADE,null=True,blank=True)


    def __str__(self):
        return str(self.id)