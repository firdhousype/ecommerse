#Author : Firdhousy P.E
#Views.py : integration of microservices
from django.shortcuts import render,redirect
import requests
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
import math
from .models import *
from .models import Zone,Unit,Pincode,Product,Image,Footwearchart,Dresschart,Products_variations,Products_variations_options,Products_options,Products_variation_combinations,Sku
from django.shortcuts import render,redirect
from Category.models import *
from Coupon.models import *
from import_export import resources
from tablib import Dataset
# ############### BULK UPLOAD PRODUCTS AND IMAGES
def uploadproduct(request):
    products=Product.objects.all().order_by('-id')
    if request.method == 'POST':
        product_resource = resources.modelresource_factory(model=Product)()
        dataset = Dataset()
        new_product = request.FILES['excel']
        imported_data = dataset.load(new_product.read())
        result = product_resource.import_data(dataset,dry_run=True)
        if not result.has_errors():
            product_resource.import_data(dataset,dry_run=False)
        return redirect('uploadproduct')
    return render(request,'upload.html',{'products':products})
def imageupload(request):
    if request.method=='POST':
        images= request.FILES.getlist('image')
        for i in images:
            proimage= BulkImages(image=i)
            proimage.save()
        # extra = {'popup':True,'heading':'Success','msg':'Successfully Uploaded Images'}
        # print("_---------------------------",extra)
        # return render(request,'image.html',{'extra':extra})
        return HttpResponse("Successfully Upload")
    return render(request,'image.html')
################ FOOT WEAR CHART CRUD OPERATION ##############
def listFoot(request):
    foot=Footwearchart.objects.all()
    if 'delete' in request.GET and request.method == 'POST':
        footid = request.POST.get('footid')
        foots=Footwearchart.objects.filter(id=footid)
        foots.delete()
    return render(request,'listfoot.html',{'foot':foot})
def addFoot(request):
    if request.method == 'POST':
        country=request.POST.get('country')
        size=request.POST.get('size')
        foot_length=request.POST.get('foot_length')
        response=Footwearchart.objects.create(country=country,size=size,foot_length=foot_length)
        return redirect('listFoot')
    return render(request,'addfoot.html')
def updateFoot(request,footid):
    foot=Footwearchart.objects.filter(id=footid)
    return render(request,'updatefoot.html',{'foot':foot})
def update_Foot(request):
    updatevalues={}
    if request.method =='POST':
        k=request.POST.get('footid')
        k=Footwearchart.objects.filter(id=k)
        country=request.POST.get('country')
        size=request.POST.get('size')
        foot_length=request.POST.get('foot_length')
        if country:
            updatevalues['country']=country
        if size:
            updatevalues['size']=size
        if foot_length:
            updatevalues['foot_length']=foot_length
        k.update(**updatevalues)
        return redirect('listFoot')
################ DRESS WEAR CHART CRUD OPERATION ##############
def listDresschart(request):
    dress=Dresschart.objects.all()
    if 'delete' in request.GET and request.method == 'POST':
        dressid = request.POST.get('dressid')
        dresses=Dresschart.objects.filter(id=dressid)
        dresses.delete()
    return render(request,'list_dress.html',{'dress':dress})
def addDresschart(request):
    if request.method == 'POST':
        country=request.POST.get('country')
        size=request.POST.get('size')
        chest=request.POST.get('chest')
        front_length=request.POST.get('front_length')
        acrross_shoulder=request.POST.get('acrross_shoulder')
        response=Dresschart.objects.create(country=country,size=size,chest=chest,front_length=front_length,acrross_shoulder=acrross_shoulder)
        return redirect('listDresschart')
    return render(request,'add_dress.html')
def updateDresschart(request,dressid):
    dress=Dresschart.objects.filter(id=dressid)
    return render(request,'updatedress.html',{'dress':dress})
def update_Dresschart(request):
    updatevalues={}
    if request.method =='POST':
        k=request.POST.get('dressid')
        k=Dresschart.objects.filter(id=k)
        country = request.POST.get('country')
        size=request.POST.get('size')
        chest=request.POST.get('chest')
        front_length=request.POST.get('front_length')
        acrross_shoulder=request.POST.get('acrross_shoulder')
        if country:
            updatevalues['country']=country
        if size:
            updatevalues['size']=size
        if chest:
            updatevalues['chest']=chest
        if front_length:
            updatevalues['front_length']=front_length
        if acrross_shoulder:
            updatevalues['acrross_shoulder']=acrross_shoulder
        k.update(**updatevalues)
        return redirect('listDresschart')
################ UNIT CRUD OPERATION ##############
def listUnit(request):
    units=Unit.objects.all()
    if 'delete' in request.GET and request.method == 'POST':
        unitid = request.POST.get('unitid')
        unit=Unit.objects.filter(id=unitid)
        unit.delete()
    return render(request,'listunit.html',{'units':units})
def addUnit(request):
    if request.method == 'POST':
        units=request.POST.get('units')
        unitname=request.POST.get('unitname')
        response=Unit.objects.create(unitname=unitname,units=units)
        return redirect('listUnit')
    return render(request,'addunit.html')
def updateunit(request,unitId):
    unit=Unit.objects.filter(id=unitId)
    return render(request,'updateunit.html',{'unit':unit})
def update_unit(request):
    updatevalues={}
    if request.method =='POST':
        k=request.POST.get('unitid')
        k=Unit.objects.filter(id=k)
        units=request.POST.get('units')
        unitname=request.POST.get('unitname')
        if units:
            updatevalues['units']=units
        if unitname:
            updatevalues['unitname']=unitname
        k.update(**updatevalues)
        return redirect('listUnit')
# ################ PINCODES CRUD OPERATION ##############
def listPincode(request):
    pincodes=Pincode.objects.all()
    if 'delete' in request.GET and request.method == 'POST':
        pincodeid = request.POST.get('pincodeid')
        pincode=Pincode.objects.filter(id=pincodeid)
        pincode.delete()
    return render(request,'listpincode.html',{'pincodes':pincodes})
def addPincode(request):
    if request.method == 'POST':
        pincode=request.POST.get('pincode')
        city=request.POST.get('city')
        state=request.POST.get('state')
        delivery_within_days=request.POST.get('delivery_within_days')
        delivery_charge=request.POST.get('delivery_charge')
        response=Pincode.objects.create(pincode=pincode,city=city,state=state,delivery_within_days=delivery_within_days,delivery_charge=delivery_charge)
        return redirect('listPincode')
    return render(request,'pincode.html')
def updatepincode(request,pinId):
    pin=Pincode.objects.filter(id=pinId)
    return render(request,'updatepincode.html',{'pin':pin})
def update_pincode(request):
    updatevalues={}
    if request.method =='POST':
        k=request.POST.get('pinId')
        k=Pincode.objects.filter(id=k)
        pincode=request.POST.get('pincode')
        city=request.POST.get('city')
        state=request.POST.get('state')
        delivery_within_days=request.POST.get('delivery_within_days')
        delivery_charge=request.POST.get('delivery_charge')
        if pincode:
            updatevalues['pincode']=pincode
        if city:
            updatevalues['city']=city
        if state:
            updatevalues['state']=state
        if delivery_within_days:
            updatevalues['delivery_within_days']=delivery_within_days
        if delivery_charge:
            updatevalues['delivery_charge']=delivery_charge
        k.update(**updatevalues)
        return redirect('listPincode')
# ################# ZONE CRUD OPERATIONS ############
def listZone(request):
    zones=Zone.objects.all()
    if 'delete' in request.GET and request.method == 'POST':
        zoneid = request.POST.get('zoneid')
        zone=Zone.objects.filter(id=zoneid)
        return redirect('listZone')
    return render(request,'listzone.html',{'zones':zones})
def addZone(request):
    zones=Zone.objects.all()
    if request.method == 'POST':
        zone_name=request.POST.get('zone_name')
        delivery_within_days=request.POST.get('delivery_within_days')
        delivery_charge=request.POST.get('delivery_charge')
        lat =    request.POST.get('lat')
        longt =    request.POST.get('long')
        d =    request.POST.get('distance')
        print("distance distance",d)
        R = 6378.1 #Radius of the Earth
        brng = 1.57 #Bearing is 90 degrees converted to radians.
        # d = 15 #Distance in km
        #lat2  52.20444 - the lat result I'm hoping for
        #lon2  0.36056 - the long result I'm hoping for.
        lat1 = float(lat) * (math.pi * 180) #Current lat po converted to radians
        lon1 = float(longt) * (math.pi * 180) #Current long point converted to radians
        lat2 = math.asin( math.sin(lat1)*math.cos(float(d)/R) +math.cos(lat1)*math.sin(float(d)/R)*math.cos(brng))
        lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(float(d)/R)*math.cos(lat1),math.cos(float(d)/R)-math.sin(lat1)*math.sin(lat2))
        response=Zone.objects.create(zone_name=zone_name,latitude=lat2,longitude=lon2,delivery_within_days=delivery_within_days,delivery_charge=delivery_charge,distance=d)
        return redirect('listZone')
    return render(request,'zone.html',{'zones':zones})
def updatezone(request,zoneId):
    zone=Zone.objects.filter(id=zoneId)
    return render(request,'updatezone.html',{'zone':zone})
def update_zone(request):
    updatevalues    =   {}
    if request.method =='POST':
        k=request.POST.get('zoneId')
        k=Zone.objects.filter(id=k)
        zone_name=request.POST.get('zone_name')
        delivery_within_days=request.POST.get('delivery_within_days')
        delivery_charge=request.POST.get('delivery_charge')
        lat =    request.POST.get('lat')
        longt =    request.POST.get('long')
        d =    request.POST.get('distance')
        R = 6378.1 #Radius of the Earth
        brng = 1.57 #Bearing is 90 degrees converted to radians.
        # d = 15 #Distance in km
        #lat2  52.20444 - the lat result I'm hoping for
        #lon2  0.36056 - the long result I'm hoping for.
        lat1 = float(lat) * (math.pi * 180) #Current lat po converted to radians
        lon1 = float(longt) * (math.pi * 180) #Current long point converted to radians
        lat2 = math.asin( math.sin(lat1)*math.cos(float(d)/R) +math.cos(lat1)*math.sin(float(d)/R)*math.cos(brng))
        lon2 = lon1 + math.atan2(math.sin(brng)*math.sin(float(d)/R)*math.cos(lat1),math.cos(float(d)/R)-math.sin(lat1)*math.sin(lat2))
        if zone_name:
            updatevalues['zone_name']=zone_name
        if lat:
            updatevalues['latitude']=lat2
        if longt:
            updatevalues['longitude']=lon2
        if delivery_within_days:
            updatevalues['delivery_within_days']=delivery_within_days
        if delivery_charge:
            updatevalues['delivery_charge']=delivery_charge
        if d:
            updatevalues['distance']=d
        k.update(**updatevalues)
        return redirect('listZone')
# ################ PRODUCT CRUD OPERATION ##############
def listProduct(request):
    products=Product.objects.all().order_by('-id')    
    if 'delete' in request.GET and request.method == 'POST':
        productid = request.POST.get('productid')
        product=Product.objects.get(id=productid)   
        sku=Sku.objects.filter(product_id__id=product.id)
        sku.delete()
        product.delete()
        return render(request,'productlist.html',{'products':products,'sku':sku})
    return render(request,'productlist.html',{'products':products})
def addProduct(request):
    category=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id')
    unit=Unit.objects.all()
    pincode=Pincode.objects.all()
    zone=Zone.objects.all()
    coupon=Couponcode.objects.all()
    if 'category' in request.GET:
        maincategory = request.GET.get('category')
        response = Category.objects.filter(parent__id=maincategory).exclude(category_name__contains="Banner").order_by('-id')
        return render(request,'sub-category.html',{'subcategory':response })
    add_product = {}
    if request.method == 'POST':
        product_name=request.POST.get('product_name')
        if product_name:
            add_product['product_name']=product_name
        product_image1=request.FILES.get('product_image1')
        if product_image1:
            add_product['product_image1']=product_image1
        product_image2=request.FILES.get('product_image2')
        if product_image2:
            add_product['product_image2']=product_image2
        product_image3=request.FILES.get('product_image3')
        if product_image3:
            add_product['product_image3']=product_image3
        product_description=request.POST.get('product_description')
        if product_description:
            add_product['product_description']=product_description
        highlights=request.POST.get('highlights')
        if highlights:
            add_product['highlights']=highlights
        return_policy=request.POST.get('return_policy')
        if return_policy:
            add_product['return_policy']=return_policy
        number_of_days_for_refund=request.POST.get('number_of_days_for_refund')
        if number_of_days_for_refund:
            add_product['number_of_days_for_refund']=number_of_days_for_refund
        Net_weight=request.POST.get('Net_weight')
        if Net_weight:
            add_product['Net_weight']=Net_weight
        banner1=request.FILES.get('banner1')
        if banner1:
            add_product['banner1']=banner1
        banner2=request.FILES.get('banner2')
        if banner2:
            add_product['banner2']=banner2
        delivery_charge=request.POST.get('delivery_charge')
        if delivery_charge:
            add_product['delivery_charge']=delivery_charge
        brand_name=request.POST.get('brand_name')
        if brand_name:
            add_product['brand_name']=brand_name
        delivery_days=request.POST.get('delivery_days')
        if delivery_days:
            add_product['delivery_days']=delivery_days
        maincategory=request.POST.get('category')
        if maincategory:
            maincategory=Category.objects.get(id=maincategory)
            add_product['maincategory']=maincategory
        subcategory=request.POST.get('subcategory')
        if int(subcategory)  !=0:
            subcategory=Category.objects.get(id=subcategory)
            add_product['subcategory']=subcategory
        coupon_code=request.POST.get('coupon_code')
        if int(coupon_code) != 0:
            coupon_code =   Couponcode.objects.filter(id=coupon_code)
            add_product['coupon_code']=coupon_code
        units=request.POST.get('units')
        if int(units) != 0:
            unit=Unit.objects.get(id=units)
            add_product['unit']=unit
        footware_sizechart=request.POST.get('footware_sizechart')
        dress_sizechart=request.POST.get('dress_sizechart')
        if dress_sizechart == 'on':
            dress_sizechart1=True
            add_product['dress_sizechart']=dress_sizechart1
        else:
            dress_sizechart1=False
            add_product['dress_sizechart']=dress_sizechart1
        if footware_sizechart == 'on':
            footware_sizechart1=True
            add_product['footware_sizechart']=footware_sizechart1
        else:
            footware_sizechart1=False
            add_product['footware_sizechart']=footware_sizechart1
        p=Product.objects.create(**add_product)
        pincodes=request.POST.getlist('pincode')
        pincode=Pincode.objects.filter(id__in=pincodes).values_list('id',flat=True)
        p.pincode.set(pincode)
        p.save()
        zones=request.POST.getlist('zone')
        zone=Zone.objects.filter(id__in=zones).values_list('id',flat=True)
        p.zone.set(zone)
        p.save()
        return redirect(skudetails,productid=p.id)
    return render(request,'addproduct.html',{'category':category,'unit':unit,'pincode':pincode,'coupon':coupon,'zone':zone})
def updateProduct(request,productId):
    if 'category' in request.GET:
        maincategory = request.GET.get('category')
        response = Category.objects.filter(parent__id=maincategory).exclude(category_name__contains="Banner").order_by('-id')
        return render(request,'sub-category.html',{'subcategory':response })
    category=Category.objects.all().exclude(category_name__contains="Banner").order_by('-id')
    unit=Unit.objects.all()
    pincode1=Pincode.objects.all()
    zone1=Zone.objects.all()
    coupon=Couponcode.objects.all()
    product=Product.objects.filter(id=productId)
    coupon1=product[0].coupon_code_id
    if coupon1 !=  None:
        coupon1=int(coupon1)
    category1=product[0].subcategory_id
    if category1 !=  None:
        category1=int(category1)
    maincategory=product[0].maincategory_id
    if maincategory !=  None:
        maincategory=int(maincategory)
    pin = []
    pin=product[0].pincode.all().values_list('id',flat=True) 
    zon = []
    zon=product[0].zone.all().values_list('id',flat=True) 
    units=product[0].unit_id
    if units != None:
        u=int(units)
        return render(request,'updateproduct.html',{'zon':zon,'coupon':coupon,'product':product,'category':category,'unit':unit,'pincode1':pincode1,'pin':pin,'u':u,'c':category1,'cp':coupon1,'mc':maincategory,'zone1':zone1})
    else:
        return render(request,'updateproduct.html',{'zon':zon,'coupon':coupon,'product':product,'category':category,'unit':unit,'pincode1':pincode1,'pin':pin,'c':category1,'cp':coupon1,'mc':maincategory,'zone1':zone1})  
def update_product(request):
    updateproduct={}
    if request.method == 'POST':
        ids=request.POST.get('productId')
        pd=Product.objects.get(id=ids)
        p=Product.objects.filter(id=ids)
        pincodes=request.POST.getlist('pincode')
        pincode=Pincode.objects.filter(id__in=pincodes).values_list('id',flat=True)
        zones=request.POST.getlist('zone')
        zone=Zone.objects.filter(id__in=zones).values_list('id',flat=True)
        k=pd.pincode.all().values_list('id',flat=True)
        if k:
            pd.pincode.remove(*k)
        pd.pincode.set(pincode)
        pd.save()
        j=pd.zone.all().values_list('id',flat=True)
        if j:
            pd.zone.remove(*j)
        pd.zone.set(zone)
        pd.save()
        product_name=request.POST.get('product_name')
        if product_name:
            updateproduct['product_name']=product_name
        product_image1=request.FILES.get('product_image1')
        if product_image1:
            updateproduct['product_image1']=product_image1
        product_image2=request.FILES.get('product_image2')
        if product_image2:
            updateproduct['product_image2']=product_image2
        product_image3=request.FILES.get('product_image3')
        if product_image3:
            updateproduct['product_image3']=product_image3
        product_description=request.POST.get('product_description')
        if product_description:
            updateproduct['product_description']=product_description
        highlights=request.POST.get('highlights')
        if highlights:
            updateproduct['highlights']=highlights
        return_policy=request.POST.get('return_policy')
        if return_policy:
            updateproduct['return_policy']=return_policy
        number_of_days_for_refund=request.POST.get('number_of_days_for_refund')
        if number_of_days_for_refund:
            updateproduct['number_of_days_for_refund']=number_of_days_for_refund
        Net_weight=request.POST.get('Net_weight')
        if Net_weight:
            updateproduct['Net_weight']=Net_weight
        banner1=request.FILES.get('banner1')
        if banner1:
            updateproduct['banner1']=banner1
        banner2=request.FILES.get('banner2')
        if banner2:
            updateproduct['banner2']=banner2
        delivery_charge=request.POST.get('delivery_charge')
        if delivery_charge:
            updateproduct['delivery_charge']=delivery_charge
        brand_name=request.POST.get('brand_name')
        if brand_name:
            updateproduct['brand_name']=brand_name
        delivery_days=request.POST.get('delivery_days')
        if delivery_days:
            updateproduct['delivery_days']=delivery_days
        maincategory=request.POST.get('category')
        if maincategory:
            maincategory=Category.objects.get(id=maincategory)
            updateproduct['maincategory']=maincategory
        subcategory=request.POST.get('subcategory')
        if int(subcategory)  !=0:
            subcategory=Category.objects.get(id=subcategory)
            updateproduct['subcategory']=subcategory
        coupon_code=request.POST.get('coupon_code')
        if int(coupon_code) != 0:
            updateproduct['coupon_code']=coupon_code
        units=request.POST.get('units')
        if int(units) != 0:
            unit=Unit.objects.get(id=units)
            updateproduct['unit']=unit
        footware_sizechart=request.POST.get('footware_sizechart')
        if footware_sizechart == 'on':
            footware_sizechart1=True
            updateproduct['footware_sizechart']=footware_sizechart1
        elif footware_sizechart == None:
            footware_sizechart1=False
            updateproduct['footware_sizechart']=footware_sizechart1
        dress_sizechart=request.POST.get('dress_sizechart')
        if dress_sizechart == 'on':
            dress_sizechart=True
            updateproduct['dress_sizechart']=dress_sizechart
        elif dress_sizechart == None:
            dress_sizechart=False
            updateproduct['dress_sizechart']=dress_sizechart
        p.update(**updateproduct)
        return redirect('listProduct') 
# #################### SKU CREATION ##################
def skudetails(request,productid):
    product_id = productid
    response=Sku.objects.filter(product_id=product_id).values()
    l=[]
    for i in response:
        # v=i['products_variation_combinations_id_id']
        # v=list(Products_variation_combinations.objects.filter(id=v).values_list('id',flat=True))
        if i['products_variation_combinations_id_id']  ==   None and i['products_variation_combinations_id_secondary_id']  == None  :
            l=[]
        elif i['products_variation_combinations_id_id']  ==   None :
            svn = i['products_variation_combinations_id_secondary_id']
            v=(Products_variation_combinations.objects.filter(id=svn).values_list('id',flat=True))
            l=list(v)
        elif i['products_variation_combinations_id_secondary_id']  ==   None :
            v=i['products_variation_combinations_id_id']
            v=(Products_variation_combinations.objects.filter(id=v).values_list('id',flat=True))
            l=list(v)
        else:
            svn = i['products_variation_combinations_id_secondary_id']
            v=i['products_variation_combinations_id_id']
            z=[]
            z.append(svn)
            z.append(v)
            v=(Products_variation_combinations.objects.filter(id__in=z).values_list('id',flat=True))
            l=list(v)            
    product=Product.objects.filter(id=productid)
    category_id=product[0].subcategory
    maincategory=product[0].maincategory
    if category_id == None:
        response1=Products_variation_combinations.objects.filter(category_id=maincategory)
        if request.method=='POST':
            price  = request.POST.getlist('combination_id1[]')
            sales_rate  = request.POST.getlist('combination_id2[]')
            stock  = request.POST.getlist('combination_id3[]')
            combination_id = request.POST.getlist('combination_id[]')
            combination_id2 = request.POST.getlist('combination_id4[]')
            L1=[]
            L1.append(price)
            L1.append(sales_rate)
            L1.append(stock)
            L1.append(combination_id)
            L1.append(combination_id2)
            N = len(L1)
            j=0
            while j < len(combination_id):
                res = [i[j] for i in L1[ : N]]
                pr=res[0]
                sr=res[1]
                sk=res[2]
                co=res[3]
                co1=res[4]
                subcategory = 0
                if int(co) != 0 and int(co1) != 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(subcategory)+'-'+str(co)
                    value = Products_variation_combinations.objects.filter(id=co)
                    value1 = Products_variation_combinations.objects.get(id=co1)
                    value2 = Products_variation_combinations.objects.filter(id=co1)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    value.update(product_id=product)
                    value2.update(product_id=product)
                    value = Products_variation_combinations.objects.get(id=co)
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product,products_variation_combinations_id=value,products_variation_combinations_id_secondary=value1)
                elif int(co) != 0 and int(co1) == 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(subcategory)+'-'+str(co)
                    value = Products_variation_combinations.objects.filter(id=co)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    value.update(product_id=product)
                    value = Products_variation_combinations.objects.get(id=co)
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product,products_variation_combinations_id=value)
                elif int(co) == 0 and int(co1) != 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(subcategory)+'-'+str(co)
                    value1 = Products_variation_combinations.objects.get(id=co1)
                    value2 = Products_variation_combinations.objects.filter(id=co1)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    value2.update(product_id=product)
                    value = Products_variation_combinations.objects.get(id=co) 
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product,products_variation_combinations_id_secondary=value1)
                elif int(co) == 0 and int(co1) == 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(subcategory)+'-'+str(co)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)

                    product=Product.objects.get(id=productid)
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product)
                j += 1
            return redirect(skudetails,productid=product_id)
    else:
        response1=Products_variation_combinations.objects.filter(category_id=category_id)
        if request.method=='POST':
            price  = request.POST.getlist('combination_id1[]')
            sales_rate  = request.POST.getlist('combination_id2[]')
            stock  = request.POST.getlist('combination_id3[]')
            combination_id = request.POST.getlist('combination_id[]')
            combination_id2 = request.POST.getlist('combination_id4[]')
            L1=[]
            L1.append(price)
            L1.append(sales_rate)
            L1.append(stock)
            L1.append(combination_id)
            L1.append(combination_id2)
            N = len(L1)
            j=0
            while j < len(combination_id):
                res = [i[j] for i in L1[ : N]]
                pr=res[0]
                sr=res[1]
                sk=res[2]
                co=res[3]
                co1=res[4]
                if int(co) != 0 and int(co1) != 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(category_id)+'-'+str(co)
                    value = Products_variation_combinations.objects.filter(id=co)
                    value1 = Products_variation_combinations.objects.get(id=co1)
                    value2 = Products_variation_combinations.objects.filter(id=co1)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    value.update(product_id=product)
                    value2.update(product_id=product)
                    value = Products_variation_combinations.objects.get(id=co)
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product,products_variation_combinations_id=value,products_variation_combinations_id_secondary=value1)
                elif int(co) != 0 and int(co1) == 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(category_id)+'-'+str(co)
                    value = Products_variation_combinations.objects.filter(id=co)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    value.update(product_id=product)
                    value = Products_variation_combinations.objects.get(id=co)
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product,products_variation_combinations_id=value)
                elif int(co) == 0 and int(co1) != 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(category_id)+'-'+str(co)
                    value1 = Products_variation_combinations.objects.get(id=co1)
                    value2 = Products_variation_combinations.objects.filter(id=co1)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    value2.update(product_id=product)
                    value = Products_variation_combinations.objects.get(id=co) 
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product,products_variation_combinations_id_secondary=value1)
                elif int(co) == 0 and int(co1) == 0:
                    sku_name=str(product_id)+'-'+str(maincategory)+'-'+str(category_id)+'-'+str(co)
                    product=Product.objects.filter(id=productid)
                    product.update(skus=True)
                    product=Product.objects.get(id=productid)
                    if pr!=0:
                        offer=(int(100-(int(sr)/int(pr))*100))
                    else:
                        offer = 0
                    response=Sku.objects.create(offer=offer,sku_name=sku_name,price=pr,stock=sk,sales_rate=sr,product_id=product)
                j += 1
            return redirect(skudetails,productid=product_id)
    return render(request,'productvariation1.html',{'response1':response1,'id':productid,'vn':l})
def deletesku(request):
    if 'delete' in request.GET and request.method == 'POST':
        skuid = request.POST.get('skuid')
        sku=Sku.objects.filter(id=skuid)
        product_id=sku[0].product_id_id
        m=Product.objects.filter(id=product_id)
        m.update(skus=False)
        skus=sku.delete()
        return redirect(retrievesku,productid=product_id)
def retrievesku(request,productid):
    product_id = productid
    response=Sku.objects.filter(product_id=product_id)
    return render(request,'sku.html',{'response':response,'id':productid})
def updatesku(request,skuid):
    sku=Sku.objects.filter(id=skuid)
    product_variation_ids    =   sku[0].products_variation_combinations_id_id
    product_variation_ids1    =   sku[0].products_variation_combinations_id_secondary
    if product_variation_ids != None:
        product_variation_ids=sku[0].products_variation_combinations_id.product_variations_options_id.product_variation_id.variation_name
        product_variation_option=sku[0].products_variation_combinations_id.product_variations_options_id.variationOptionName
    else:
        product_variation_ids = 0
        product_variation_option = 0
    if product_variation_ids1 != None:
        product_variation_ids1=sku[0].products_variation_combinations_id_secondary.product_variations_options_id.product_variation_id.variation_name
        product_variation_option1=sku[0].products_variation_combinations_id_secondary.product_variations_options_id.variationOptionName
    else:
        product_variation_ids1 = 0
        product_variation_option1 = 0
    product_id=sku[0].product_id_id
    product=Product.objects.filter(id=product_id)
    category_id=product[0].subcategory_id
    maincategory=product[0].maincategory_id
    if category_id == None:
        print("NO SUBCATEGORY")
        response1=Products_variation_combinations.objects.filter(category_id=maincategory)
        return render(request,'skuupdate.html',{'sku':sku,'response1':response1,'product_variation_id':str(product_variation_ids),'product_variation_option':str(product_variation_option),'product_variation_id1':str(product_variation_ids1),'product_variation_option1':str(product_variation_option1)})
    else:
        print("SUBCategory exist")
        response1=Products_variation_combinations.objects.filter(category_id=category_id)
        return render(request,'skuupdate.html',{'sku':sku,'response1':response1,'product_variation_id':str(product_variation_ids),'product_variation_option':str(product_variation_option),'product_variation_id1':str(product_variation_ids1),'product_variation_option1':str(product_variation_option1)})
def update_sku(request):
    update_sku  =   {}
    if request.method=='POST':
        skuid=request.POST.get('skuid')
        sku_=Sku.objects.filter(id=skuid)
        sku_name = request.POST.get('sku_name')
        if sku_name:
            update_sku['sku_name']=sku_name
        price  = request.POST.get('price')
        if price:
            update_sku['price']=price
        sales_rate  = request.POST.get('sales_rate')
        if sales_rate:
            update_sku['sales_rate']=sales_rate
        stock  = request.POST.get('stock')
        if stock:
            update_sku['stock']=stock
        product_id=request.POST.get('product_id')   
        if product_id:
            product=Product.objects.get(id=product_id)
            update_sku['product_id']=product
        combination_id = request.POST.get('combination_id')
        if int(combination_id) != 0:
            value = Products_variation_combinations.objects.get(id=combination_id)
            update_sku['products_variation_combinations_id'] = value
        combination_id1 = request.POST.get('combination_id1')
        if int(combination_id1) != 0:
            value = Products_variation_combinations.objects.get(id=combination_id1)
            update_sku['products_variation_combinations_id_secondary'] = value
        if price and sales_rate:
            offer=(int(100-(int(sales_rate)/int(price))*100))
            update_sku['offer'] = offer
        sku_.update(**update_sku)
        return redirect(retrievesku,productid=product_id)
def uploadsku(request):
    if request.method == 'POST':
        new_sku = request.FILES['excel']
        sku_resource = resources.modelresource_factory(model=Sku)()
        dataset = Dataset()
        imported_data = dataset.load(new_sku.read())
        result = sku_resource.import_data(dataset,dry_run=True)
        if not result.has_errors():
            sku_resource.import_data(dataset,dry_run=False)
        wb = openpyxl.load_workbook(new_sku)
        worksheet = wb["Sheet1"]
        excel_data = list()
        for row in worksheet.iter_rows():
            row_data = list()
            for cell in row:
                row_data.append(str(cell.value))
            excel_data.append(row_data)
        N = len(excel_data)
        productid   =   [i[1] for i in excel_data[ : N]]
        maincat     =   [i[12] for i in excel_data[ : N]]
        subcat      =   [i[13] for i in excel_data[ : N]]
        comid       =   [i[26] for i in excel_data[ : N]]
        a=1
        sku=[]
        while a <= int(N-1):
            k= productid[a]+'-'+maincat[a]+'-'+subcat[a]+'-'+comid[a] 
            sku.append(k)
            a += 1
        cd =comid
        del cd[0]
        j=0
        while j < len(cd):
            skuname=Sku.objects.filter(products_variation_combinations_id__in=cd)
            for i in skuname:
                print("777",i.sku_name)
                i.sku_name = sku[j]
                i.save()
                j += 1
        return redirect('uploadsku')
    return render(request,'uploadsku.html',{'products':products})
################ PRODUCT VARIATIONS CRUD OPERATION ##############
def listProductVariations(request):
    product_variations      =   Products_variations.objects.all().order_by('-id')
    pv                      =   Products_variations.objects.all().order_by('-id')
    if 'delete' in request.GET and request.method == 'POST':
        pvid                =   request.POST.get('pvid')
        response1           =   Products_variations.objects.filter(id=pvid)
        response2           =   Products_variations_options.objects.filter(product_variation_id__id=response1[0].id)
        response2.delete()
        response1.delete()
        return redirect('listProductVariations')
    return render(request,'ProductVariations.html',{'product_variations':product_variations,'pv':pv})
def addProductVariationsname(request):
    product_variations      =   Products_variations.objects.all().order_by('-id')
    if request.method       == 'POST':
        product_variation_id    =   request.POST.get('product_variation_id')
        pid                 =   Products_variations.objects.get(id=product_variation_id)
        variationOptionName =   request.POST.get('variationOptionName')
        response1           =   Products_variations_options.objects.create(product_variation_id=pid,variationOptionName=variationOptionName)
        return redirect('listProductVariations')
    return render(request,'addProductVariations.html',{'product_variations':product_variations})
def addProductVariations(request):
    product_variations      =   Products_variations.objects.all().order_by('-id')
    if request.method       == 'POST':
        variation_name      =   request.POST.get('variation_name')
        response            =   Products_variations.objects.create(variation_name=variation_name)
        return redirect('addProductVariationsname')
    return render(request,'addProductVariations.html',{'product_variations':product_variations})

def updateProductVariations(request,pvid):
    product_variations      =   Products_variations.objects.all()
    pv                      =   Products_variations.objects.filter(id=pvid)
    pvn                     =   Products_variations_options.objects.filter(product_variation_id__id=pv[0].id)
    return render(request,'updateProductVariations.html',{'pv':pv,'pvn':pvn,'product_variations':product_variations,'ids':pv[0].id})
def update_ProductVariations(request):
    if request.method =='POST':
        k=request.POST.get('pvid')
        variation_name=request.POST.get('variation_name')
        response=Products_variations.objects.filter(id=k)
        response.update(variation_name=variation_name)
        return redirect('updateProductVariations',pvid=k)
def update_ProductVariationsname(request):
    updates={}
    if request.method =='POST':
        k=request.POST.get('pvid')
        response1=Products_variations_options.objects.filter(id=k)
        product_variation_id=request.POST.get('product_variation_id')
        # print("11111111111111",product_variation_id)
        # if product_variation_id:
        #     response=Products_variations.objects.filter(id__in=product_variation_id)
        #     updates['product_variation_id']=response
        variationOptionName=request.POST.get('variationOptionName')
        if variationOptionName:
            updates['variationOptionName']=variationOptionName
        response1.update(**updates)
        return redirect('listProductVariations')


