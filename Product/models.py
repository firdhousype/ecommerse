from django.db import models
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
import PIL
from PIL import Image
from django.utils.safestring import mark_safe
from django.template.defaultfilters import truncatechars
from django.db.models import Avg

# Create your models here.
class Zone(models.Model):
    zone_name               =   models.CharField(max_length=50,null=True,blank=True)
    latitude                =   models.CharField(max_length=20,null=True,blank=True)
    longitude               =   models.CharField(max_length=20,null=True,blank=True)
    delivery_within_days    =   models.IntegerField(null=True,blank=True)
    delivery_charge         =   models.IntegerField(null=True,blank=True)
    distance                =   models.IntegerField(null=True,blank=True)
    def __str__(self):
       return self.zone_name

class Unit(models.Model):
    unitname                =   models.CharField( max_length=50, null=True)
    units                   =   models.CharField( max_length=50, null=True)
    def __str__(self):
      return self.units

class Pincode(models.Model):
    pincode                 =   models.CharField(max_length=20,null=True,blank=True)
    city                    =   models.CharField(max_length=20,null=True,blank=True)
    state                   =   models.CharField(max_length=20,null=True,blank=True)
    delivery_within_days    =   models.IntegerField(null=True,blank=True)
    delivery_charge         =   models.IntegerField(null=True,blank=True)
    def __str__(self):
       return self.pincode

class Product(models.Model):
    product_name            =   models.CharField(max_length=20)
    product_image1          =   models.ImageField(upload_to='products',blank=True,null=True)
    product_image2          =   models.ImageField(upload_to='products',blank=True,null=True)
    product_image3          =   models.ImageField(upload_to='products',blank=True,null=True)
    product_description     =   RichTextField(null=True,blank=True)
    highlights              =   RichTextField(null=True,blank=True)
    return_policy           =   RichTextField(null=True,blank=True)
    number_of_days_for_refund=  models.IntegerField(null=True,blank=True)
    Net_weight              =   models.CharField(max_length=50,null=True,blank=True)
    order_customer_location =   models.BooleanField(default=True)
    comment                 =   models.BooleanField(default=False)
    minimum_product_percentage= models.IntegerField(null=True,blank=True)
    delivery_date           =   models.DateField(null=True)
    banner1                 =   models.ImageField(upload_to='products',blank=True,null=True)
    banner2                 =   models.ImageField(upload_to='products',blank=True,null=True)
    footware_sizechart      =   models.BooleanField(default=False)
    dress_sizechart         =   models.BooleanField(default=False)
    delivery_charge         =   models.IntegerField(null=True,blank=True)
    brand_name              =   models.CharField(max_length=50,null=True,blank=True)
    refund                  =   models.BooleanField(default=False)
    delivery_days           =   models.IntegerField(null=True,blank=True)
    maincategory            =   models.ForeignKey('Category.Category',on_delete=models.CASCADE,null=True,blank=True)
    subcategory             =   models.ForeignKey('Category.Category',on_delete=models.CASCADE,null=True,blank=True,related_name='Subcategory')
    unit                    =   models.ForeignKey('Unit',on_delete=models.CASCADE,null=True,blank=True)
    coupon_code             =   models.ForeignKey('Coupon.Couponcode',on_delete=models.CASCADE,null=True,blank=True)
    pincode                 =   models.ManyToManyField('Pincode')
    zone                    =   models.ManyToManyField('Zone')
    skus         =   models.BooleanField(default=False)

    @property
    def short_description(self):
      return truncatechars(self.description,20)

    def product_photo(self):
       return mark_safe('<img src="{}" width="100" />'.format(self.product_image1.url))
    product_photo.short_description = 'Image'
    product_photo.allow_tags= True


    def __str__(self):
       return self.product_name
	
    def get_avg_rating(self):
       reviews = Rating.objects.filter(product_ID=self).aggregate(rating_avg=Avg('rating'))
       return (reviews['rating_avg'])

class Image(models.Model):
     image      =   models.ImageField(upload_to='products',blank=True,null=True)
class BulkImages(models.Model):
     image      =   models.ImageField(upload_to='products',blank=True,null=True)

class Footwearchart(models.Model):
    country     =   models.CharField(max_length=100,null=True)
    size        =   models.CharField(max_length=100,null=True,blank=True)
    foot_length =   models.CharField(max_length=100,null=True)
    def __str__(self):
        return str(self.id)

class Dresschart(models.Model):
    country             =       models.CharField(max_length=100,null=True)
    size                =       models.CharField(max_length=100,null=True)
    chest               =       models.IntegerField(null=True,blank=True)
    front_length        =       models.CharField(max_length=100,null=True,blank=True)
    acrross_shoulder    =       models.CharField(max_length=100,null=True,blank=True)
    def __str__(self):
        return str(self.id)
################## Product Options ################
class Products_variations(models.Model):
    variation_name = models.CharField(max_length=100,null=True,blank=True)
    def __str__(self):
        return str(self.variation_name)
        
class Products_variations_options(models.Model):
    product_variation_id    =   models.ForeignKey('Products_variations',on_delete=models.CASCADE,null=True,blank=True)
    variationOptionName     =   models.CharField(max_length=250,null=True,blank=True)
    def __str__(self):
        return str(self.variationOptionName)
class Products_options(models.Model):
    product_id              =   models.ForeignKey('Product',on_delete=models.CASCADE,null=True,blank=True)
    category_id             =   models.ForeignKey('Category.Category',on_delete=models.CASCADE,null=True,blank=True)
    product_variation_id    =   models.ForeignKey('Products_variations',on_delete=models.CASCADE,null=True,blank=True)    
    def __str__(self):
        return str(self.id)
class Products_variation_combinations(models.Model):
    product_id              =   models.ForeignKey('Product',on_delete=models.CASCADE,null=True,blank=True)
    category_id             =   models.ForeignKey('Category.Category',on_delete=models.CASCADE,null=True,blank=True)
    product_variations_options_id = models.ForeignKey('Products_variations_options',on_delete=models.CASCADE,null=True,blank=True)
    product_option_id       =   models.ForeignKey('Products_options',on_delete=models.CASCADE,null=True,blank=True)
    def __str__(self):
        return str(self.id)
############### SKU ###############
class Sku(models.Model):
    sku_name                            =   models.CharField(max_length=200,null=True,blank=True)
    product_id                          =   models.ForeignKey('Product',on_delete=models.CASCADE,null=True,blank=True)
    price                               =   models.IntegerField(null=True,blank=True)
    sales_rate                          =   models.IntegerField(null=True,blank=True)
    offer                               =   models.IntegerField(null=True)
    stock                               =   models.IntegerField(null=True,blank=True)
    stock_status                        =   models.BooleanField(default=False,null=True,blank=True)
    products_variation_combinations_id  =   models.ForeignKey('Products_variation_combinations',on_delete=models.CASCADE,null=True,blank=True)
    products_variation_combinations_id_secondary = models.ForeignKey('Products_variation_combinations',on_delete=models.CASCADE,null=True,blank=True,related_name="Products_variation_combinations_secondary")
    def __str__(self):
        return str(self.sku_name)
