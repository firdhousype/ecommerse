from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('listProduct',    views.listProduct,    name = 'listProduct'),
    path('addProduct',views.addProduct,name='addProduct'),
    path('updateProduct/<int:productId>',views.updateProduct,name='updateProduct'),
    path('update_product',views.update_product,name='update_product'),
# ######## Zone   #########
    path('listZone',views.listZone,name='listZone'),
    path('addZone',views.addZone,name='addZone'),
    path('updatezone/<int:zoneId>',views.updatezone,name='updatezone'),
    path('update_zone',views.update_zone,name='update_zone'),
# ######## UNIT   #########
    path('listUnit',views.listUnit,name='listUnit'),
    path('addUnit',views.addUnit,name='addUnit'),
    path('updateunit/<int:unitId>',views.updateunit,name='updateunit'),
    path('update_unit',views.update_unit,name='update_unit'),
# ########### PINCODE     ################
    path('listPincode',views.listPincode,name='listPincode'),
    path('addPincode',views.addPincode,name='addPincode'),
    path('updatepincode/<int:pinId>',views.updatepincode,name='updatepincode'),
    path('update_pincode',views.update_pincode,name='update_pincode'),
################ FOOT #############
    path('listFoot',views.listFoot,name='listFoot'),
    path('addFoot',views.addFoot,name='addFoot'),
    path('updateFoot/<int:footid>',views.updateFoot,name='updateFoot'),
    path('update_Foot',views.update_Foot,name='update_Foot'),
# ################ DRESS  #############
    path('listDresschart',views.listDresschart,name='listDresschart'),
    path('addDresschart',views.addDresschart,name='addDresschart'),
    path('updateDresschart/<int:dressid>',views.updateDresschart,name='updateDresschart'),
    path('update_Dresschart',views.update_Dresschart,name='update_Dresschart'),
# ######### PRODUCT VARIATIONS ##########
    path('listProductVariations',views.listProductVariations,name='listProductVariations'),
    path('addProductVariations',views.addProductVariations,name='addProductVariations'),
    path('updateProductVariations/<int:pvid>',views.updateProductVariations,name='updateProductVariations'),
    path('update_ProductVariations',views.update_ProductVariations,name='update_ProductVariations'),
    path('addProductVariationsname',views.addProductVariationsname,name='addProductVariationsname'),
    path('update_ProductVariationsname',views.update_ProductVariationsname,name='update_ProductVariationsname'),
    
# #################### Upload products and Images ###################
    path('uploadproduct',views.uploadproduct,name="uploadproduct"),
    path('imageupload',views.imageupload,name="imageupload"),
# ############## SKU_CREATIONS ############
    path('skudetails/<int:productid>',views.skudetails,name='skudetails'),
    path('retrievesku/<int:productid>',views.retrievesku,name='retrievesku'),
    path('updatesku/<int:skuid>',views.updatesku,name='updatesku'),
    path('update_sku',views.update_sku,name='update_sku'),
    path('deletesku',views.deletesku,name='deletesku'),
    path('uploadsku',views.uploadsku,name='uploadsku'),
]

