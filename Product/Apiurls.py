from django.contrib import admin
from django.urls import path,include
from . import ApiView
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('sizechart/<str:sizechart>',ApiView.sizechart,name='size'), ############# same using for footwear
    path('listProducts',ApiView.listProducts,name='listProducts'), 
    path('RetrieveProducts/<int:productid>',ApiView.RetrieveProducts,name='RetrieveProducts'),
    path('retrieveproduct_bycategory/<int:catid>',ApiView.products_by_category,name='products_by_category'),

]