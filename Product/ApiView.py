from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
from rest_framework.response import Response 
from rest_framework.decorators import api_view,permission_classes
from .serializers import *
from .models import *
@api_view(['GET'])
def sizechart(request,sizechart):
    if sizechart=='dress_sizechart':
        d=Dresschart.objects.all()
        serializer=dresserializer(d,many=True)
        return Response(serializer.data)
    elif sizechart=='footware_sizechart':
        f=Footwearchart.objects.all()
        serializer=footserializer(f,many=True)
        return Response(serializer.data)
    else:
        return Response('no size chart for this input')
@api_view(['GET'])
def listProducts(request):
    p=Product.objects.all().exclude(skus=False).order_by('-id')
    serializer=productserializer(p,many=True)
    return Response (serializer.data)
@api_view(['GET'])
def RetrieveProducts(request,productid):
    p=Product.objects.filter(id=productid)
    serializer=productserializer(p,many=True)
    return Response (serializer.data)
@api_view(['GET'])
def products_by_category(request,catid):
    p=Product.objects.filter(maincategory=catid).order_by('-id')
    serializer=productserializer(p,many=True)
    return Response (serializer.data)