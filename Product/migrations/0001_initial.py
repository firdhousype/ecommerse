# Generated by Django 3.2.4 on 2021-07-12 12:04

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Category', '0001_initial'),
        ('Coupon', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dresschart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(max_length=100, null=True)),
                ('size', models.CharField(max_length=100, null=True)),
                ('chest', models.IntegerField(blank=True, null=True)),
                ('front_length', models.CharField(blank=True, max_length=100, null=True)),
                ('acrross_shoulder', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Footwearchart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(max_length=100, null=True)),
                ('size', models.CharField(blank=True, max_length=100, null=True)),
                ('foot_length', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to='products')),
            ],
        ),
        migrations.CreateModel(
            name='Pincode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pincode', models.CharField(blank=True, max_length=20, null=True)),
                ('city', models.CharField(blank=True, max_length=20, null=True)),
                ('state', models.CharField(blank=True, max_length=20, null=True)),
                ('delivery_within_days', models.IntegerField(blank=True, null=True)),
                ('delivery_charge', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_name', models.CharField(max_length=20)),
                ('product_image1', models.ImageField(blank=True, null=True, upload_to='products')),
                ('product_image2', models.ImageField(blank=True, null=True, upload_to='products')),
                ('product_image3', models.ImageField(blank=True, null=True, upload_to='products')),
                ('product_description', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('highlights', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('return_policy', ckeditor.fields.RichTextField(blank=True, null=True)),
                ('number_of_days_for_refund', models.IntegerField(blank=True, null=True)),
                ('Net_weight', models.CharField(blank=True, max_length=50, null=True)),
                ('order_customer_location', models.BooleanField(default=True)),
                ('comment', models.BooleanField(default=False)),
                ('minimum_product_percentage', models.IntegerField(blank=True, null=True)),
                ('delivery_date', models.DateField(null=True)),
                ('banner1', models.ImageField(blank=True, null=True, upload_to='products')),
                ('banner2', models.ImageField(blank=True, null=True, upload_to='products')),
                ('footware_sizechart', models.BooleanField(default=False)),
                ('dress_sizechart', models.BooleanField(default=False)),
                ('delivery_charge', models.IntegerField(blank=True, null=True)),
                ('brand_name', models.CharField(blank=True, max_length=50, null=True)),
                ('refund', models.BooleanField(default=False)),
                ('delivery_days', models.IntegerField(blank=True, null=True)),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Category.category')),
                ('coupon_code', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Coupon.couponcode')),
                ('pincode', models.ManyToManyField(to='Product.Pincode')),
            ],
        ),
        migrations.CreateModel(
            name='Products_options',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Category.category')),
                ('product_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.product')),
            ],
        ),
        migrations.CreateModel(
            name='Products_variation_combinations',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Category.category')),
                ('product_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.product')),
                ('product_option_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.products_options')),
            ],
        ),
        migrations.CreateModel(
            name='Products_variations',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('variation_name', models.CharField(blank=True, max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unitname', models.CharField(max_length=50, null=True)),
                ('units', models.CharField(max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('zone_name', models.CharField(blank=True, max_length=50, null=True)),
                ('latitude', models.CharField(blank=True, max_length=20, null=True)),
                ('longitude', models.CharField(blank=True, max_length=20, null=True)),
                ('delivery_within_days', models.IntegerField(blank=True, null=True)),
                ('delivery_charge', models.IntegerField(blank=True, null=True)),
                ('distance', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sku',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sku_name', models.CharField(blank=True, max_length=200, null=True)),
                ('product_id', models.CharField(blank=True, max_length=200, null=True)),
                ('price', models.IntegerField(blank=True, null=True)),
                ('sales_rate', models.IntegerField(blank=True, null=True)),
                ('offer', models.IntegerField(null=True)),
                ('stock', models.IntegerField(blank=True, null=True)),
                ('stock_status', models.BooleanField(blank=True, default=False, null=True)),
                ('products_variation_combinations_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.products_variation_combinations')),
            ],
        ),
        migrations.CreateModel(
            name='Products_variations_options',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('variationOptionName', models.CharField(blank=True, max_length=250, null=True)),
                ('product_variation_id', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.products_variations')),
            ],
        ),
        migrations.AddField(
            model_name='products_variation_combinations',
            name='product_variations_options_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.products_variations_options'),
        ),
        migrations.AddField(
            model_name='products_options',
            name='product_variation_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.products_variations'),
        ),
        migrations.AddField(
            model_name='product',
            name='unit',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='Product.unit'),
        ),
        migrations.AddField(
            model_name='product',
            name='zone',
            field=models.ManyToManyField(to='Product.Zone'),
        ),
    ]
