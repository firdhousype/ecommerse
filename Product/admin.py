from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(Products_options)
admin.site.register(Products_variation_combinations)
admin.site.register(Sku)
admin.site.register(Product)