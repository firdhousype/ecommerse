# from django.http.response import HttpResponse
# from django.shortcuts import render

import pandas as pd
import os
import requests
from pathlib import Path
import xlwt
import xlsxwriter
from django.shortcuts import render,redirect
from Product.models import Product, Sku
from Customer.models import User
from Order.models import order, orderdetails
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
# BASE_DIR = Path(__file__).resolve(strict=True).parent.parent
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# #*********************************list products ********************************#
def reportlistproduct(request):
    product=Product.objects.all().order_by('-id')
    return render(request,'reportproducts.html',{'products':product})
# #********************************repot search products***************************#
def reportsearchproduct(request):  
    path= os.path.join(BASE_DIR,"media/reports/report.xlsx")
    writer = pd.ExcelWriter(path, engine='xlsxwriter')
    df= pd.DataFrame(Product.objects.all().values())
    product=Product.objects.all()
    if request.method =='POST':
        product_name=request.POST.get('product_name')
        if product_name :
            df= pd.DataFrame(Product.objects.filter(product_name__startswith=product_name).values())
            product=Product.objects.filter(product_name__startswith=product_name)
    df2 = {'price':'','sales_rate':'','stock':'','product_variation_combinations_id':''}
    df = df.append(df2, ignore_index = True)
    df.to_excel(writer, sheet_name='Sheet1',index=False)
    worksheet=writer.sheets['Sheet1']
    for idx,col in enumerate(df):
        series=df[col]
        max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
        worksheet.set_column(idx,idx,max_len)
        writer.save()
    return render(request,"reportproducts.html",{'products':product})  
    # return render(request,"reportproducts.html",{'products':product})
# #**********************************************report customer*******************************************3
def reportlistcustomer(request):
    users=User.objects.filter(is_superuser=False).order_by('-id')
    
    
    return render(request,'reportcustomer.html',{'response':users})
# #*******************************************************report search customer *****************************************#
def reportsearchcustomer(request):  
    path= os.path.join(BASE_DIR,"media/reports/reports.csv")
    # writer = pd.ExcelWriter(path, engine='xlsxwriter')
    customer_name={}
    customer=User.objects.filter(is_superuser=False).values()
    if 'customer_name'  in request.GET:
        customer_name=request.GET['customer_name']
        print("xxxxxxxxxxx",customer_name)
        if customer_name !='':
            customer=User.objects.filter(username__startswith=customer_name).values()
            print("xxxxxxxxxxx",customer)
        elif customer_name =='':
            customer=User.objects.filter(is_superuser=False).values()
        df= pd.DataFrame(customer)
        # df2 = {'username':'','first_name':'','last_name':'','email':'','phone':'','address':'','pincode':'','role':'','city':''}
        
        # print("zzzzzzzzzzzzzzzzzzzzzzzz",df2)
        # df = df.append(df2, ignore_index = True)
        
        df.to_csv(path)
        
        # worksheet=writer.sheets['Sheet1']
        # for idx,col in enumerate(df):
        #     series=df[col]
        #     max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
        #     worksheet.set_column(idx,idx,max_len)
        # writer.save()
    else:
        df= pd.DataFrame(customer)
        # df2 = {'username':'','first_name':'','last_name':'','email':'','phone':'','address':'','pincode':'','role':'','city':''}
        
        # print("yyyyyyyyyyyyyyyyyyyyy",df2)
        # df = df.append(df2, ignore_index = True)
        
        df.to_csv(path)
        
        
        # worksheet=writer.sheets['Sheet1']
        # for idx,col in enumerate(df):
        #     series=df[col]
        #     max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
        #     worksheet.set_column(idx,idx,max_len)
        # writer.save()
    

    return render(request,"reportcustomer.html",{'response':customer})
# #********************************************************report list sales *************************************************#
def reportlistsales(request):
    
    response = order.objects.all()
    
    return render(request,"reportsales.html",{'orders':response}) 
# #********************************************************report search sales ************************************************#
def salesreportsearch(request):
    path= os.path.join(BASE_DIR,"media/reports/report1.xlsx")
    writer = pd.ExcelWriter(path, engine='xlsxwriter')
    df =pd.DataFrame(order.objects.all().values())
    orders=order.objects.all()
    if request.method=='POST':
        son=request.POST.get("ordernumber")
        s1=request.POST.get("startingdate")
        s2=request.POST.get("endingdate")
        status1=request.POST.get("status")
        
        if (s1 and s2 )!='' and status1==''and son=='' :
            df=pd.DataFrame(order.objects.filter(order_date__range=(s1,s2)).values())
            orders = order.objects.filter(order_date__range=(s1,s2))
            
            
        elif (s1 and s2 )==''and status1!='' and son=='' :
            df=pd.DataFrame(order.objects.filter(status=status1).values())
            orders = order.objects.filter(status=status1)
            
            
        elif(s1 and s2 )==''and status1=='' and son!='' :
            df=pd.DataFrame(order.objects.filter(id=son).values())
            orders=order.objects.filter(id=son)
            
            
        elif(s1 and s2 )!=''and status1!='' and son!='' :
            df=pd.DataFrame(order.objects.filter(id=son,status=status1,order_date__range=(s1,s2)).values())
            orders=order.objects.filter(id=son,status=status1,order_date__range=(s1,s2))
            
            
        elif(s1 and s2 )!=''and status1!='' and son=='' :
            df=pd.DataFrame(order.objects.filter(status=status1,order_date__range=(s1,s2)).values())
            orders=order.objects.filter(status=status1,order_date__range=(s1,s2))
            
            
        elif(s1 and s2 )==''and status1!='' and son!='' :
            df=pd.DataFrame(order.objects.filter(status=status1,id=son).values())
            orders=order.objects.filter(status=status1,id=son)
           
            
        elif(s1 and s2 )!=''and status1=='' and son!='' :
            df=pd.DataFrame(order.objects.filter(order_date__range=(s1,s2),id=son).values())
            orders=order.objects.filter(id=son,order_date__range=(s1,s2))
            
    # df2 = {'id':'','order_date':'','order_total':'','subtotal':'','customer_id':'','product_id':'','paid':''}
        
        
    # df = df.append(df2, ignore_index = True)
    df.to_excel(writer, sheet_name='Sheet1',index=False)
    worksheet=writer.sheets['Sheet1']
    for idx,col in enumerate(df):
        series=df[col]
        max_len=max((series.astype(str).map(len).max(),len(str(series.name))))+1
        worksheet.set_column(idx,idx,max_len)
    writer.save()
    
           
    
    return render(request,"reportsales.html",{'orders':orders})

