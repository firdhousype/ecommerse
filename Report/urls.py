from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
#     # path('generete-report',    views.reports,    name = 'reports'),
    path('reportlistproduct',views.reportlistproduct,name="reportlistproduct"),
    path('reportsearchproduct',views.reportsearchproduct,name="reportsearchproduct"),
    path('reportcustomer',views.reportlistcustomer,name="reportlistcustomer"),
    path('reportsearchcustomer',views.reportsearchcustomer,name="reportsearchcustomer"),
    path('reportlistsales',views.reportlistsales,name="reportlistsales"),
    path('salesreportsearch',views.salesreportsearch,name="salesreportsearch"),
    
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)