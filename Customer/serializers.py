from rest_framework import serializers
from .models import User
class userserializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=['id',
        'email',
        'username',
        # 'password',
        'address',
        'postcode',
        # 'status',
        'role',
        'city',
        'phone',
        'DOB',
        'xCordinate',
        'yCordinate'
        ]