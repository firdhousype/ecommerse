#author -- Firdhousy P.E
# In this views.py, done the crud operatoins for user using django ORM.
from django.http.response import HttpResponse
from django.shortcuts import render
import re
import urllib
### for rest_framwork ###
from rest_framework.response import Response 
from rest_framework.decorators import api_view,permission_classes
from .models import User
from .serializers import userserializer
from django.contrib.auth import authenticate,login,logout
from rest_framework import status
from django.contrib.auth.hashers import make_password
### for token ###
from rest_framework.authtoken.views  import ObtainAuthToken
from  rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
import requests

# from pyfcm import FCMNotification

### token based login ##

class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_id': user.pk,
            #'email': user.email,
            'username':user.username
        })

################################################ ADDUSER##################################
@api_view(['POST'])
def addUser(request):
    # push_service = FCMNotification(api_key="AAAAxURh5Ac:APA91bGNoDH-i6RTSAg-bZxu-FDdgMbsjnNh59FZUVk0nJ_PMc3bMhy-ez7HMlJktDTDxg_h4x_PL32jNC6FhYnRXxj3zFBrSl_UKGclcxY0_cZxK1FLBIVFwmuDzMpAJzExp04vuHiP")
    if request.method=='POST':
        email=request.data.get('email')
        username=request.data.get('username')
        password=request.data.get('password')
        address=request.data.get('address')
        postcode=request.data.get('postcode')
        city=request.data.get('city')
        phone=request.data.get('phone')
        dob=request.data.get('DOB')
        staff_name=request.data.get('staff_name')
        xcordinate=request.data.get('xCordinate')
        ycordinate=request.data.get('yCordinate')
        if User.objects.filter(phone=phone).exists():
            return Response("Phone Number Already Exist")
        # newToken = FirebaseInstanceId.getInstance().getToken()
        # # newToken = device registration_id
        # registration_id = newToken
        # message_title = "Succefully Registerd"
        # message_body = "Hi Firdhousy, Succefully created a new account"
        # result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title, message_body=message_body)
        # print("----------------------------------------",result)
        
        k=User.objects.create_user(email=email,username=username,password=password,address=address,postcode=postcode,city=city,phone=phone,DOB=dob,staff_name=staff_name,xCordinate=xcordinate,yCordinate=ycordinate)
        
        

        ad=User.objects.filter(id=k.id)
        serializer=userserializer(ad,many=True)
        return Response(serializer.data)
######################################## LOGIN ############################################


def logout(request):
    # logout(request)
    return render(request,'login.html')
####################################### EDIT_USER_DETAILS #################################
@api_view(['PUT'])
def updateUser(request,userid):
    k=User.objects.filter(id=userid).values()
    if request.method=='PUT':
        email=request.data.get('email')
        updatevalues={}
        if email:
            updatevalues["email"]=email
        username=request.data.get('username')
        if username:
            updatevalues["username"]=username
        address=request.data.get('address')
        if address:
            updatevalues["address"]=address
        postcode=request.data.get('postcode')
        if postcode:
            updatevalues["postcode"]=postcode
        DOB=request.data.get('DOB')
        if DOB:
            updatevalues["DOB"]=DOB
        city=request.data.get('city')
        if city:
            updatevalues["city"]=city
        phone=request.data.get('phone')
        if phone:
            updatevalues["phone"]=phone
        try :
            xaxis=request.data.get('xCordinate')
            yaxis=request.data.get('yCordinate')
            updatevalues["xCordinate"]=xaxis
            updatevalues["yCordinate"]=yaxis
        except :
            print ('No axis details')
        k.update(**updatevalues )
        ad=User.objects.filter(id=userid)
        serializer=userserializer(ad,many=True)
        return Response(serializer.data)
    return Response('please fill the form correctly')
################################### DELETE_USER ###############################################
@api_view(['GET'])
def deleteUser(request,userid):
    u=User.objects.get(id=userid)
    u.delete()
    return Response("Successfully deleted")
###################################  Logout Users###############################################
@api_view(['GET'])
def logoutview(request):
    logout(request)
    return Response({'status':"successfully logout"},status=status.HTTP_200_OK)
######################################list all Users############################################
@api_view(['GET'])
def listUsers(request):
    users=User.objects.all()
    serializer=userserializer(users,many=True)
    return Response(serializer.data)
####################################search User By Name###################################
@api_view(['GET'])
def searchUser(request,name):
    users= User.objects.filter(username__contains=name)
    serializer=userserializer(users,many=True)
    return Response (serializer.data)

@api_view(['GET'])
def listuserscount(request):
    users=User.objects.filter(is_superuser=False).all().count()
    return Response(users)
##################################list Customer except super User ########################

def listcustomers(request):
    users=User.objects.filter(is_superuser=False)
    return render(request,'list_users.html',{'response':users})
@api_view(['GET'])
def retrieveUser(request,orderid):
    name = User.objects.filter(id=orderid)
    serializer = userserializer(name,many=True)
    return Response(serializer.data)

# @api_view(['POST'])
# def adduserstatus(request):
#     if request.method=='POST':
#       userid=request.data.get('userid')
#       status = request.data.get('status')
#       u=User.objects.get(id=userid)
#       u.status=status
#       u.save()
#       serializer=userserializer(u,many=True)
#       return Response(serializer.data)

@api_view(['POST'])
def checkUser(request):
    number = request.data.get('phone')
    if (isValid(number)):  
        user_data=User.objects.filter(phone=number).values()
        updateUserPassword={}
        #otp=random.randint(1000,9999)
        otp=123
        # message='Your OTP is '+str(otp)
        message=str(otp)+'%20is%20your%20verification%20code%20%n-%20CYDEZ'
        try:
            user_phone = user_data[0]['phone']
        except:
            user_phone = ''
        if user_phone:
            password=str(otp)
            password = make_password(password)
            updateUserPassword["password"]=password
            updateUserPassword["otp"]=otp
            user_data.update(**updateUserPassword )
            #resp =  sendSMS('y7OuqTQobwg-2Br8J4uLsrYomuRkM9vYI3vyIrq0k2', number,'ASPRIT', message)
            resp =  sendSMS('0eDrqgX5mzs-k86Ucw3hmKBaRa9qXixwR8nCwDKsMT', number,'CYDEZT', message)
            return Response({'status':"Existing User"},status=status.HTTP_200_OK)
        else:
            password=str(otp)
            registerUsingPhone(number,password,otp)
            resp =  sendSMS('y7OuqTQobwg-2Br8J4uLsrYomuRkM9vYI3vyIrq0k2',number,'ASPRIT', message)
            return Response({'status':"Registered New User"},status=status.HTTP_200_OK)
        return Response(otp)    
    else : 
        return Response("In Valid Number")

def sendSMS(apikey, numbers, sender, message):
    data =  urllib.parse.urlencode({'apikey': apikey, 'numbers': numbers,
        'message' : message, 'sender': sender})
    data = data.encode('utf-8')
    request = urllib.request.Request("https://api.textlocal.in/send/?")
    f = urllib.request.urlopen(request, data)
    fr = f.read()

def registerUsingPhone(phone,password,otp):
        registered_mobile=User.objects.create_user_phone(phone=phone,password=password,otp=otp)
        return registered_mobile   
def isValid(s): 
    Pattern = re.compile("^[7-9][0-9]{9}$")
    return Pattern.match(s) 


@api_view(['POST'])
def userResetPasword(request):
    number = request.data.get('phone')
    password = request.data.get('password')
    PhoneOtp = request.data.get('otp')
    user_data=User.objects.filter(phone=number).values()
    updateUserPassword={}
 
    try:
        user_otp = str(user_data[0]['otp'])
    except:
        user_otp = 0
    if (isValid(number) and (user_otp == PhoneOtp)):  
        user_data=User.objects.filter(phone=number).values()
        updateUserPassword={}
        try:
            user_phone = user_data[0]['phone']
        except:
            user_phone = ''
        if user_phone:
            password = make_password(password)
            updateUserPassword["password"]=password
            user_data.update(**updateUserPassword )
        else:
            return Response("Not a registered user")
        return Response("Password changed successfully")    
    else : 
        return Response("In Valid Number")

# @api_view(['POST'])
# def userzone(request):
#     if request.method == 'POST':
#         lat=request.POST.get('x-coordinate')
#         lon=request.POST.get('y-coordinate')
#         zone=requests.post(product_url+'searchzone',data={'x-coordinate':lat,'y-coordinate':lon}).json()
#         return Response(zone)

