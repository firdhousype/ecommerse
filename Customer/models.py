from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class UserManager(BaseUserManager):
       
    def _create_user(self, phone, password, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not phone:
            raise ValueError('Valid Mobile number must be given')
       # email = self.normalize_email(email)
        
        user = self.model(phone=phone, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def _create_user_phone(self, phone, password,otp, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not phone:
            raise ValueError('Phone number is mandatory')
        
        user = self.model(phone=phone,password=password,otp=otp, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, phone, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **other_fields)
    
    def create_user_phone(self, phone, password,otp, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user_phone(phone, password,otp,**other_fields)

    def create_superuser(self, phone, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **other_fields)

class User(AbstractUser): 
    username    =   models.CharField(max_length=100,null=True,blank=True)
    first_name  =   models.CharField(max_length=100,null=True,blank=True)
    last_name   =   models.CharField(max_length=100,null=True,blank=True)
    email       =   models.EmailField(max_length=50, null=True,blank=True)
    phone       =   models.IntegerField(unique=True,null=True,blank=True)
    address     =   models.TextField(max_length=100,null=True,blank=True)
    city        =   models.CharField(max_length=255,null=True,blank=True)
    postcode    =   models.IntegerField(null=True,default=None,blank=True)
    role        =   models.CharField(max_length=30,null=True,blank=True)
    otp         =   models.IntegerField(null=True,default=None)
    xCordinate  =   models.CharField(max_length=255,null=True,blank=True)
    yCordinate  =   models.CharField(max_length=255,null=True,blank=True)
    DOB         =   models.DateField(null=True)
    staff_name  =   models.CharField(max_length=100,null=True,blank=True)
    image       =   models.ImageField(upload_to='customer', blank=True, null=True,default='default.png')

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['address','email','city','postcode','role','staff_name','image']

    objects=UserManager()

    def get_username(self):
        return str(self.first_name)
