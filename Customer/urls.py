from django.contrib import admin
from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
############### USER CRUD OPERATIONS #################
    path('login', views.CustomAuthToken.as_view(),name='login'), ######### token based login
    path('addUser',views.addUser,name='addUser'),
    path('logout',views.logout,name='logout'),
    path('updateUser/<int:userid>',views.updateUser,name='updateUser'),
    path('deleteUser/<int:userid>',views.deleteUser,name='deleteUser'),
    path('logoutview',views.logoutview,name='logoutview'),
    path('listUsers',views.listUsers,name='listUsers'),
    path('searchUser/<str:name>',views.searchUser,name='searchUser'),
    path('usercount',views.listuserscount,name="usercount"),
    path('listcustomers',views.listcustomers,name="listcustomers"),
    path('retrieveUser/<int:orderid>',views.retrieveUser,name="retrieveUser"),
    # path('adduserstatus',views.adduserstatus,name="adduserstatus"),
    path('check-user', views.checkUser, name='checkUser'),
    path('reset-password', views.userResetPasword, name='checkUser'),
    # path('userzone',views.userzone,name='userzone'),
    ]